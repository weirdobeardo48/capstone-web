use Capstone;

/*================userTable===========================*/

							/*  all password are T-team123    */

insert into userTable(userName, userPassword, userPhone, userEmail, userType, isActive) 
				values ('adminaccount', '079E76C0A5ECFEA075780EAEF8A9DD4F', '01234567890', 'admin@gmail.com', 0, 1);
insert into userTable(userName, userPassword, userPhone, userEmail, userType, isActive) 
				values ('modaccount1', '079E76C0A5ECFEA075780EAEF8A9DD4F', '01234567891', 'mod1@gmail.com', 2, 1);
insert into userTable(userName, userPassword, userPhone, userEmail, userType, isActive) 
				values ('modaccount2', '079E76C0A5ECFEA075780EAEF8A9DD4F', '01257733432', 'mod2@gmail.com', 2, 1);                
insert into userTable(userName, userPassword, userPhone, userEmail, userType, isActive) 
				values ('useraccount1', '079E76C0A5ECFEA075780EAEF8A9DD4F', '01234567892', 'user1@gmail.com', 1, 1);
insert into userTable(userName, userPassword, userPhone, userEmail, userType, isActive) 
				values ('useraccount2', '079E76C0A5ECFEA075780EAEF8A9DD4F', '01234567893', 'user2@gmail.com', 1, 1);

                
/*===================packageTable========================*/

insert into packageTable(packageID, packageName, packageDescription, packagePrice, DaysOfUsing) 
				values(1,'Package 1','This is description of package P1',60000,30);
insert into packageTable(packageID, packageName, packageDescription, packagePrice, DaysOfUsing) 
				values(2,'Package 2','This is description of package P2',90000,60);
insert into packageTable(packageID, packageName, packageDescription, packagePrice, DaysOfUsing) 
				values(3,'Package 3','This is description of package P3',150000,120);
                
                
/*===================subscriptiontable========================*/  
insert into subscriptionTable(userID, expiredDate, isExpired)
				values(4, '2019-09-28', 0);
insert into subscriptionTable(userID, expiredDate, isExpired)
				values(5, '2017-04-28', 0);

/*====================Application_Version=======================*/
insert into Application_Version(appVersionNumber, changeLog, appLink)
				values('0.1', 'alpha version', 'www.facebook.com');
insert into Application_Version(appVersionNumber, changeLog, appLink)
				values('0.2', 'beta version', 'apple.com');
insert into Application_Version(appVersionNumber, changeLog, appLink)
				values('1.3', 'omega version', 'fb.me');

/*====================Data_Version=======================*/
insert into Data_Version(dataVersionNumber)
				values(0.1);
