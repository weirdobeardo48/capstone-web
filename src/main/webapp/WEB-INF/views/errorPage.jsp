<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Errors</title>
</head>
<body>
	<jsp:include page="header.jsp" />
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h1>404</h1>
				<br />
				<p>Oops! The page you requested was not found!</p>
				<p>Or something happened! We will fix it as soon as possible!</p>
				<p>
					<a href="mailto:contact@cfml-contentfilter.com?subject=Error Report">Contact
						us</a> about this error now!
				</p>

			</div>
		</div>
	</div>
	<jsp:include page="footer.jsp" />
</body>
</html>