<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta charset="utf-8">
<!-- Meta tags and CSS reference needed for bootstrap -->
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta name="viewport" content="width=device-width, initial-scale=1.0">


<title>Log in</title>
</head>

<body>
<jsp:include page="header.jsp" />
<div class="container form-page" >
 	<hr width="100%"/>
 	<h2>Log in</h2>
    
    <form id="loginForm" action="<c:url value="/login.xyz"/>" method="POST" style="margin-top: -10px">
                <div class="row" style="margin-top: 10px; margin-bottom: 20px">
			<div class="col-md-12">
					<c:if test="${not empty error}"><i style="color: red;">${error}</i></c:if>
					<c:if test="${not empty success}"><i style="color: green;">${success}</i></c:if>
			</div>
			</div>
                <div class="row">
                    <div class="col-md-12">
                        <input type="text" class="form-control" placeholder="Username" name="inputUserName" id="inputUserName" autofocus
                        value="<c:if test="${not empty user}">${user.userName}</c:if>" required="required" />
                    </div>
                </div>
                <br />
                <div class="row">
                    <div class="col-md-12">
                        <input type="password" class="form-control" placeholder="Password" name="inputPassword" id="inputPassword" required="required" />
                    </div>
                </div>
                <br />
                <div class="row" style="margin-top: 10px">
                    <div class="col-md-12" align="center">
                        <input type="submit" value="Log in" class="btn btn-primary.gradient" name="loginButton" id="loginButton" style="width:100%"/>
                    </div>
                </div>
                <div class="row log" style="margin-top: 20px">
                    <div class="col-md-12" align="center">
                    	<p>
                            <a href="<c:url value="/reset"/>">Forgot password</a>
                        </p>
                    </div>
                </div>
                <hr width="100%"/>
    			<div class="row log" style="margin-top: -33px;">
                    <div class="col-md-12" align="center">
                    	<p>
                            <a href="<c:url value="/register"/>" style=" background-color:white">&nbsp;&nbsp;Register&nbsp;&nbsp;</a>
                        </p>
                    </div>
    </div>
               
    </form>
    </div>
    
    <jsp:include page="footer.jsp" />
</body>
</html>