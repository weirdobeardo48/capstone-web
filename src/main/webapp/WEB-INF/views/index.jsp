<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta charset="utf-8">
<!-- Meta tags and CSS reference needed for bootstrap -->
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta name="viewport" content="width=device-width, initial-scale=1.0">


<title>Home</title>
</head>

<body style="padding:0px; font-family:'Lato'; overflow-x: hidden;">
	<jsp:include page="header.jsp" />
		<a name="top"></a>
		<div class="download">
			<div class="row">
				<div class="col-md-1"></div>
				<div class="col-md-4" style="margin-left: 60px">
					<h1>CFML</h1>
					<h4>Protect your children <br/>from pornographic contents</h4>
					<div class="row" style="margin-top: 30px">
						<div class="col-md-5">
							<img src="<c:url value="/contents/img/Icon/Image.png" />"/>&nbsp;&nbsp;&nbsp;Images
						</div>
						<div class="col-md-5">
							<img src="<c:url value="/contents/img/Icon/Video.png" />"/>&nbsp;&nbsp;&nbsp;Videos
						</div>
					</div>
					<a class="register" href="${link}"><button class="downloadBtn" align="center">DOWNLOAD</button></a>
					
				</div>
				
			</div>
		</div>
		
		<div class="advantage" align="center">
			<h1>CFML ADVANTAGES</h1>
			<div class="row" style="margin-top: 50px">
			<div class="col-md-4">
				<img src="<c:url value="/contents/img/Icon/Working offline.png" />" style="width: 34%"/>
				<h3>Working offline</h3>
				<h4>CFML works well at <br/>censoring screen <br/>even when there is no <br/>Internet connection</h4>
			</div>
			<div class="col-md-4">
				<img src="<c:url value="/contents/img/Icon/Data update frequently.png" />" style="width: 34%"/>
				<h3>Updating data frequently</h3>
				<h4>Data is updated once a <br/>month to help you to <br/>protect your children <br/>more effectively</h4>
			</div>
			<div class="col-md-4">
				<img src="<c:url value="/contents/img/Icon/keep good content.png" />" style="width: 33%"/>
				<h3>Keeping good contents</h3>
				<h4>Only pornographic <br/>contents displaying on <br/>the screen will be <br/>censored</h4>
			</div>
			</div>
		</div>
		<div class="vip" align="center">
			<h1>VIP ACCOUNT</h1>
			<div class="content">
			<div class="row" style="margin-top: 50px">
			<div class="col-md-4">
				<img src="<c:url value="/contents/img/Icon/control privacy.png" />" style="width: 20%"/>
				<h3>Controlling privacy</h3>
				<h4>Data from user<br/>device will not be<br/>collected without<br/>user's agreement</h4>
			</div>
			<div class="col-md-4">
				<img src="<c:url value="/contents/img/Icon/mute sound.png" />" style="width: 25%"/>
				<h3>Muting sound</h3>
				<h4>CFML mutes the<br/>applications when it<br/>detected pornographic<br/>contents</h4>
			</div>
			<div class="col-md-4">
				<img src="<c:url value="/contents/img/Icon/Check inactive applications.png" />" style="width: 25%"/>
				<h3>Checking inactive apps</h3>
				<h4>Inactive applications<br/>will be minimized<br/>when pornographic<br/>contents are inside</h4>
			</div>
			</div>
			<div class="row" style="margin-top: 50px">
			<div class="col-md-1" style="width:14%"></div>
			<div class="col-md-4">
				<img src="<c:url value="/contents/img/Icon/Customize censor box.png" />" style="width: 25%"/>
				<h3>Customizing censor box</h3>
				<h4>User can insert any<br/>image on censor boxes<br/>and change color of<br/>its background</h4>
			</div>
			<div class="col-md-4">
				<img src="<c:url value="/contents/img/Icon/Sale.png" />" style="width: 25%"/>
				<h3>Getting sales</h3>
				<h4>VIP account will be<br/>updated information<br/>about package sales<br/>as soon as possible</h4>
			</div>
			</div>
			</div>
		</div>
		
		<div class="experience" align="center">
			<h1>USER EXPERIENCES</h1>
			<div class="row" style="margin-top: 50px">
				<div class="col-md-4">
					<img src="<c:url value="/contents/img/Icon/User1.png" />" style="width: 60%"/>
				</div>
				<div class="col-md-8">
					<div class="note" align="left">I usually concerned if my children was safe when <br/> they surfing the Internet or not. Since I used CFML, <br/>I am totally satisfied.</div>
					<div class="peo" align="right">Mr. Phuc Nghia</div>
					<div class="pos" align="right">CEO - Hanoi</div>
				</div>
			</div>
			<div class="row" style="margin-top: 50px">
				<div class="col-md-4">
					<img src="<c:url value="/contents/img/Icon/User2.png" />" style="width: 65%"/>
				</div>
				<div class="col-md-8">
					<div class="note" align="left">I used to use some of anti-porn products but they<br/>seemed impossible to prevent contents on pictures<br/>and videos. To CFML, it turns to be possible.</div>
					<div class="peo" align="right">Mrs. Nhat Huyen</div>
					<div class="pos" align="right">Developer - Hue</div>
				</div>
			</div>
		</div>
		<div class="contact" align="center"> 
			<h1>CONTACT</h1>
			<br/>
			<h2>T-TEAM</h2>
			<div class="row">
				<div class="col-md-6" align="right" style="border-right: 2px solid white">Tel: 01248449150</div>
				<div class="col-md-6" align="left">Fax: 0204 012 345</div>
			</div>
			<div class="row">
				<div class="col-md-6" align="right" style="border-right: 2px solid white">Address: Hoa Lac Hi-tech park, Hanoi</div>
				<div class="col-md-6" align="left">E-mail: contact@cfml-contentfilter.com</div>
			</div>
		</div>
		<jsp:include page="footer.jsp" />
</body>
<script>

$('a[href*="#"]')
// Remove links that don't actually link to anything
.not('[href="#"]')
.not('[href="#0"]')
.click(function(event) {
  // On-page links
  if (
    location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') 
    && 
    location.hostname == this.hostname
  ) {
    // Figure out element to scroll to
    var target = $(this.hash);
    target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
    // Does a scroll target exist?
    if (target.length) {
      // Only prevent default if animation is actually gonna happen
      event.preventDefault();
      $('html, body').animate({
        scrollTop: target.offset().top
      }, 1000, function() {
        // Callback after animation
        // Must change focus!
        var $target = $(target);
        $target.focus();
        if ($target.is(":focus")) { // Checking if the target was focused
          return false;
        } else {
          $target.attr('tabindex','-1'); // Adding tabindex for elements not focusable
          $target.focus(); // Set focus again
        };
      });
    }
  }
});
</script>
</html>