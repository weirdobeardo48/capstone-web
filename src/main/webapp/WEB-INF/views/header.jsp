<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@page import="capstone.model.UserTable"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<html>
<head>
<script>
  function moneyconvert(money) {
    document.write(money.toLocaleString());
  }
  function moneydisplay(money) {
	    document.write(money/1000+"K");
	  }
</script>
<!-- Meta tags and CSS reference needed for bootstrap -->
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<link rel="stylesheet" type="text/css"
	href="<c:url value="/resources/css/bootstrap.min.css" />" />
<link rel="stylesheet" type="text/css"
	href="<c:url value="/contents/css/style.css" />" />
	<script src="<c:url value="/resources/js/jquery-3.2.1.min.js" />"></script>
<script src="<c:url value="/resources/js/bootstrap.min.js" /> "></script>
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.2/css/jquery.dataTables.min.css"></style>
<link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Lato" />
<script type="text/javascript" src="https://cdn.datatables.net/1.10.2/js/jquery.dataTables.min.js"></script>

</head>
<script>
$('a.page-scroll').bind('click', function(event) {
    var $anchor = $(this);
    $('html, body').stop().animate({
        scrollTop: $($anchor.attr('href')).offset().top
    }, 1500, 'easeInOutExpo');
    event.preventDefault();
    window.history.pushState(null, null, $($anchor.attr('href')).selector);
});
</script>
<body>
	<!-- Fixed navbar -->
	<nav class="navbar navbar-default navbar-fixed-top"
		style="vertical-align: middle; height: 8vh">
		<div class="container" style="height: 100%"> 
			<!-- Brand and toggle get grouped for better mobile display -->
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed"
					data-toggle="collapse" data-target="#bs-example-navbar-collapse-1"
					aria-expanded="false">
					<span class="sr-only">Toggle navigation</span> <span
						class="icon-bar"></span> <span class="icon-bar"></span> <span
						class="icon-bar"></span>
				</button>
				<c:if test="${0 != sessionScope.currentUser.userType && 2!=sessionScope.currentUser.userType}">
				<a class="navbar-brand" href="<c:url value="/index"/>">CFML</a>
				</c:if>
				<c:if test="${2 == sessionScope.currentUser.userType}">
				<a class="navbar-brand" href="<c:url value="/admin"/>">CFML</a>
				</c:if>
				<c:if test="${0==sessionScope.currentUser.userType}">
				<a class="navbar-brand" href="<c:url value="/root"/>">CFML</a>
				</c:if>
			</div>

			<!-- Collect the nav links, forms, and other content for toggling -->
			<div class="collapse navbar-collapse"
				id="bs-example-navbar-collapse-1">
				<c:if test="${0 != sessionScope.currentUser.userType && 2!=sessionScope.currentUser.userType}">
				
					<c:if test="${empty link}">
						<ul class="nav navbar-nav">
						<li><a href="<c:url value="/index"/>">Home</a></li>
						<li><a href="<c:url value="/price"/>">Package</a></li>
						<li><a href="<c:url value="/index"/>">Download</a></li>
						</ul>
					</c:if>
					<c:if test="${not empty link}">
						<ul class="nav navbar-nav">
						<li><a href="<c:url value="/index"/>">Home</a></li>
						<li><a href="<c:url value="/price"/>">Package</a></li>
						<li><a href="<c:url value="#top"/>">Download</a></li>
						</ul>
					</c:if>
				
				</c:if>
				<ul class="nav navbar-nav navbar-right" >
					<c:choose>
						<c:when test="${null != sessionScope.currentUser}">
							<li class="dropdown"><a href="#" class="dropdown-toggle"
								data-toggle="dropdown" role="button" aria-haspopup="true"
								aria-expanded="false" style="margin-top: -1vh"> Hi,
									${sessionScope.currentUser.userName} 
									<c:if
										test="${null != sessionScope.currentUser.getSubscriptionTable()}">
										<c:if test="${0 != sessionScope.currentUser.userType && 2!=sessionScope.currentUser.userType}">
											<br/>Expired date: ${sessionScope.currentUser.getSubscriptionTable().getExpiredDate()}
										</c:if>
									</c:if> <span class="caret"></span>
							</a>
								<ul class="dropdown-menu">
									<li><a href="<c:url value="/panelPage.xyz"/>">View
											profile</a></li>
									<li role="separator" class="divider"></li>
									<li><a href="<c:url value="/logout" />">Log out</a></li>
								</ul></li>

						</c:when>

						<c:otherwise>
							<li id="login"><a href="<c:url value="/login"/>">Log in</a></li>
						</c:otherwise>
					</c:choose>
				</ul>
			</div>
			<!-- nav-collapse -->
		</div>
	</nav>
</body>

</html>