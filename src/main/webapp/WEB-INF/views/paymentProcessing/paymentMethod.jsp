<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" type="text/css"
	href='<c:url value="/resources/css/bootstrap.min.css" />' />
<link rel="stylesheet" type="text/css"
	href='<c:url value="/contents/css/style.css" />' />
<link rel="stylesheet" type="text/css"
	href="font-awesome/css/font-awesome.min.css" />
<title>Payment method</title>
</head>
<body>
	<!-- Header -->
	<jsp:include page="../header.jsp" />

	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h2>Choose a payment method</h2>
				<i><h5>Click one of the options below</h5></i> <br />
				<div class="row">
					<div class="col-md-3"></div>
					<div class="col-md-3">
						<div class="hoverDiv">
							<a
								href="<c:url value="/paymentNganluong"><c:param name="price" value="${price}" ></c:param> </c:url> ">
								<img src='<c:url value="/contents/img/nganluong_english.png" />'
								width="100%" />
							</a>
						</div>
					</div>
					<div class="col-md-3">
						<div class="hoverDiv">
							<a
								href="<c:url value="/paymentBaoKim"><c:param name="price" value="${price}" ></c:param> </c:url> ">
								<img src='<c:url value="/contents/img/baokim.jpg" />'
								width="100%" />
							</a>
						</div>
					</div>
					<div class="col-md-3"></div>
				</div>

			</div>
		</div>
	</div>
	<!-- Footer -->
	<jsp:include page="../footer.jsp" />
</body>