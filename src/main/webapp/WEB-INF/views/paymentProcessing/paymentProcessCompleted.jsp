<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" type="text/css"
	href='<c:url value="/resources/css/bootstrap.min.css" />' />
<link rel="stylesheet" type="text/css"
	href='<c:url value="/contents/css/style.css" />' />
<link rel="stylesheet" type="text/css"
	href="font-awesome/css/font-awesome.min.css" />
<title>Payment method</title>
</head>
<style>
@mixin vertical-align($position: relative) {
  position: $position;
  top: 50%;
  -webkit-transform: translateY(-50%);
  -ms-transform: translateY(-50%);
  transform: translateY(-50%);
}
</style>
<body style="background-color: black">
<div class="thank">
	<!-- Header -->
	<jsp:include page="../header.jsp" />

	<div id="wrapper" class="animated zoomIn">
  <!-- We make a wrap around all of the content so that we can simply animate all of the content at the same time. I wanted a zoomIn effect and instead of placing the same class on all tags, I wrapped them into one div! -->
<h3>
	Your transaction is processed successfully.    
</h3>
<h1>
  <!-- The <h1> tag is the reason why the text is big! -->
  <underline>Thank you!</underline>
  <!-- The underline makes a border on the top and on the bottom of the text -->
</h1>
<h3>
	Thank you for using CMFL application.
</h3>
</div>
</div>


	<!-- Footer -->
	<jsp:include page="../footer.jsp" />
</body>