<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta charset="utf-8">
<!-- Meta tags and CSS reference needed for bootstrap -->
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet" type="text/css">
<link rel="stylesheet prefetch" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/5.0.0/normalize.min.css">
<title>Package</title>
</head>
<style>
        body {
            -webkit-font-smoothing: antialiased;
            
        }
        
        section {
            background: #647df9;
            color: #7a90ff;
            padding: 2em 0 8em;
            min-height: 100vh;
            position: relative;
            -webkit-font-smoothing: antialiased;
        }
        
    </style>
<body onload="<c:if test="${not empty error}">alert('${error}')</c:if>" style="font-family:'Lato'; background-color: black">
	<jsp:include page="header.jsp" />
	<div class="container" style="margin-top: 10px; margin-bottom: 0px; width: 100%;">
		<h1 align="center" style="color: white"><b>Our Packages</b></h1>
	<c:if test="${not empty packList}">
	<div class="pricing pricing--karma" style="margin-top: 10px">
                <div class="pricing__item">
                    <h3 class="pricing__title">${packList.get(0).packageName}</h3>
                    <p class="pricing__sentence"> ${packList.get(0).daysOfUsing} DAYS OF USING</p>
                    <div class="pricing__price"><span class="pricing__currency">VND</span><script>moneydisplay(${packList.get(0).packagePrice})</script><span class="pricing__period">/ ${packList.get(0).daysOfUsing} days</span></div>
                    <div class="des">${packList.get(0).packageDescription}</div>
                    
                <a href="<c:url value="/paymentProcessing/choosePaymentMethod"><c:param name="price" value="${packList.get(0).packagePrice}"></c:param></c:url>"> 
                      <button class="pricing__action">BUY NOW</button>
                </a>
                    
                   
                </div>
                <div class="pricing__item pricing__item--featured">
                    <h3 class="pricing__title">${packList.get(1).packageName}</h3>
                    <p class="pricing__sentence"> ${packList.get(1).daysOfUsing} DAYS OF USING</p>
                    <div class="pricing__price"><span class="pricing__currency">VND</span><script>moneydisplay(${packList.get(1).packagePrice})</script><span class="pricing__period">/ ${packList.get(1).daysOfUsing} days</span></div>
                    <div class="des">${packList.get(1).packageDescription}</div>
                     <a href="<c:url value="/paymentProcessing/choosePaymentMethod"><c:param name="price" value="${packList.get(1).packagePrice}"></c:param></c:url>"> 
                <button class="pricing__action">BUY NOW</button>
                </a>
                </div>
                <div class="pricing__item">
                    <h3 class="pricing__title">${packList.get(2).packageName}</h3>
                    <p class="pricing__sentence"> ${packList.get(2).daysOfUsing} DAYS OF USING</p>
                    <div class="pricing__price"><span class="pricing__currency">VND</span><script>moneydisplay(${packList.get(2).packagePrice})</script><span class="pricing__period">/ ${packList.get(2).daysOfUsing} days</span></div>
                    <div class="des">${packList.get(2).packageDescription}</div>
                     <a href="<c:url value="/paymentProcessing/choosePaymentMethod"><c:param name="price" value="${packList.get(2).packagePrice}"></c:param></c:url>"> 
                    <button class="pricing__action">BUY NOW</button>
                    </a>
                </div>
            </div>
	
	
	
			</c:if>
		<!--//End Pricing -->
	</div>
	<jsp:include page="footer.jsp" />
</body>
</html>