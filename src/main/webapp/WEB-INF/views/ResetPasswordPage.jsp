<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Register</title>
<meta charset="utf-8">
<!-- Meta tags and CSS reference needed for bootstrap -->
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta name="viewport" content="width=device-width, initial-scale=1.0">


</head>
<body>
	<jsp:include page="header.jsp" />
	<div class="container form-reset">
		<hr width="100%"/>
		<h2>Reset password</h2>
		<form id="registerForm" action="<c:url value="/reset.xyz"/>"
			method="POST" class="form-horizontal">
			<div class="row" style="margin-bottom: 20px">
			<div class="col-md-12">
				<c:if test="${not empty error}"><i style="color: red;">${error}</i></c:if>
				<c:if test="${not empty success}"><i style="color: green;">${success}</i></c:if>
				</div>
			</div>
			<div class="row">
				
				<div class="col-md-4"><strong>Username</strong></div>
				<div class="col-md-8">
					<input type="text" class="form-control" placeholder="Enter username"
						name="inputUserName" id="inputUserName" required="required" value="<c:if test="${not empty user}">${user.userName}</c:if>"autofocus />
				</div>
				<div class="col-md-6"></div>
			</div>
			<br/>
			<!--  -->
			<div class="row">
			<c:if test="${not empty errorValidMail}"><i style="color: red;">${errorValidMail}</i></c:if>
				<div class="col-md-4"><strong>E-mail</strong></div>
				<div class="col-md-8">
					<input type="email" class="form-control" placeholder="Enter e-mail you registered for the username above"
						name="inputEmail" id="inputEmail" required="required" value="<c:if test="${not empty user}">${user.userEmail}</c:if>" />
				</div>
				<div class="col-md-6"></div>
			</div>
			
			<br />
			<div class="row">
				<div class="col-md-4"></div>
				<div class="col-md-8">
					<input type="submit" value="Reset password"
						class="btn btn-primary.gradient" name="resetButton" style="width:100%; background-color:#2c90de; color: white"
						id="resetButton" />
				</div>
				<div class="col-md-6"></div>
			</div>
			<br />
		</form>
	</div>
	<jsp:include page="footer.jsp" />
</body>
</html>