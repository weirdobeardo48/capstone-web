<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">

<link rel="stylesheet" type="text/css"
	href='<c:url value="/resources/css/bootstrap.min.css" />' />
<link rel="stylesheet" type="text/css"
	href='<c:url value="/contents/css/style.css" />' />
<link rel="stylesheet" type="text/css"
	href="font-awesome/css/font-awesome.min.css" />

<title>User</title>
</head>

<body>
	<jsp:include page="../header.jsp" />
	<!-- 	CONTENT WILL BE SHOW HERE -->
	<div class="userPage">
		<div class="row">
			<div class="col-md-3">
				<jsp:include page="userSidebar.jsp" />
			</div>
			<div class="col-md-8" style="margin-left: 3%">
				<div class="row">

					<c:choose>
						<c:when test="${not empty userProfile}">
							<h1 class="page-header">User profile</h1>
							<jsp:include page="userProfile.jsp" />
						</c:when>
						<c:when test="${not empty userProfilePassword}">
							<h1 class="page-header">Change password</h1>
							<jsp:include page="userProfilePassword.jsp" />
						</c:when>
						<c:when test="${null != listTransaction}">
							<h1 class="page-header">Transaction list</h1>
							<jsp:include page="userTransaction.jsp" />
						</c:when>
					</c:choose>

				</div>
			</div>
		</div>
	</div>
	<jsp:include page="../footer.jsp" />
</body>
</html>