<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">

<link rel="stylesheet" type="text/css"
	href='<c:url value="/resources/css/bootstrap.min.css" />' />
<link rel="stylesheet" type="text/css"
	href='<c:url value="/contents/css/style.css" />' />
<link rel="stylesheet" type="text/css"
	href="font-awesome/css/font-awesome.min.css" />

<title>Admin</title>
</head>
<body>
	<!-- HEADER -->
	<jsp:include page="../header.jsp" />
	<!-- 	CONTENT WILL BE SHOW HERE -->
	<div class="rootPage">
		<div class="row">
			<div class="col-md-3">
				<jsp:include page="rootSidebar.jsp" />
			</div>
			<div class="col-md-8" style="margin-left: 3%">
				<div class="row">

					<c:choose>
					<c:when test="${not empty userProfilePassword}">
							<h1 class="page-header">Update new password</h1>
							<jsp:include page="profile/rootProfilePassword.jsp" />
						</c:when>
					<c:when test="${not empty userProfile}">
							<h1 class="page-header">Update profile</h1>
							<jsp:include page="profile/rootProfile.jsp" />
						</c:when>
						
						
						<c:when test="${not empty user}">
							<h1 class="page-header">Moderator detail</h1>
							<jsp:include page="user/rootUserDetail.jsp" />
						</c:when>
						<c:when test="${null != userListNav}">
							<h1 class="page-header">Moderator management</h1>
							<jsp:include page="user/rootUserManagement.jsp" />
						</c:when>
						<c:when test="${not empty register}">
							<h1 class="page-header">Add new moderator</h1>
							<jsp:include page="user/rootAddUser.jsp" />
						</c:when>
						<c:when test="${null != appListNav}">
							<h1 class="page-header">Application version management</h1>
							<jsp:include page="app/applicationVersionManagement.jsp" />
						</c:when>
						
						<c:when test="${not empty app}">
							<h1 class="page-header">Application version detail</h1>
							<jsp:include page="app/applicationVersionDetail.jsp" />
						</c:when>
						
						<c:when test="${not empty addApp}">
							<h1 class="page-header">Add new application version</h1>
							<jsp:include page="app/addApplicationVersion.jsp" />
						</c:when>
					</c:choose>

				</div>
			</div>
		</div>
	</div>
	<!-- FOOTER -->
	<jsp:include page="../footer.jsp" />
</body>
</html>