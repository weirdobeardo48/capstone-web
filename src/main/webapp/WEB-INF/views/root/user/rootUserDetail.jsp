<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%--
  Created by IntelliJ IDEA.
  User: zLife
  Date: 28/9/2017
  Time: 9:42 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<html>
<head>
</head>
<body>
		<div class="row" style="margin-top: 10px; margin-bottom: 20px">
			<div class="col-md-12">
					<c:if test="${not empty error}"><i style="color: red;">${error}</i></c:if>
					<c:if test="${not empty success}"><i style="color: green;">${success}</i></c:if>
			</div>
		</div>
	<c:url value="/root/users/${user.hashCode()}/save" var="requestUrl" />
	<form:form class="form-horizontal" action="${requestUrl}" method="post">
		<div class="form-group">
			<div class="col-md-3">
				<label class="control-label" >User ID</label>
			</div>
			<div class="col-md-5">
				<input class="form-control" value="${user.userID}" readonly="true" />
			</div>
		</div>

		<div class="form-group">
			<div class="col-md-3">
				<label class="control-label">Username</label>
			</div>
			<div class="col-md-5">
				<input class="form-control" value="${user.userName}" readonly="true" />
			</div>
		</div>


		<div class="form-group">
			<div class="col-md-3">
				<label class="control-label">User e-mail</label>

			</div>
			<div class="col-md-5">
				<input class="form-control" type="email" required="required" name="userEmail" value="${user.userEmail}" />
			</div>
		</div>

		<div class="form-group">
			<div class="col-md-3">
				<label class="control-label">User phone number</label>
			</div>
			<div class="col-md-5">
				<input type="number" required="required" class="form-control" name="userPhone" value="${user.userPhone}" />
			</div>
		</div>





		<div class="form-group">
			<div class="col-md-3">
				<label class="control-label">Status</label>
			</div>
			<div class="col-md-5">
				<input type="radio" name="userIsActive" value="1" <c:if test="${user.isActive	 == 1}">checked</c:if>/>
				Enable &nbsp;&nbsp;&nbsp;&nbsp;
				<input type="radio" name="userIsActive"  value="0" <c:if test="${user.isActive == 0}">checked</c:if>/>
				Disable
			</div>
		</div>

		<div class="form-group">
			<div class="col-md-3"></div>
			<div class="col-md-5">
				<input type="submit" value="Update" class="btn btn-primary.gradient" />
				<input type="reset" value="Reset" class="btn btn-primary.gradient" />
			</div>
		</div>

	</form:form>
</body>
</html>
