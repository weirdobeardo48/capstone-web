<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" type="text/css"
	href='<c:url value="/resources/css/bootstrap.min.css" />' />
<link rel="stylesheet" type="text/css"
	href='<c:url value="/contents/css/style.css" />' />
<title>Change User Password</title>
</head>
<body>
	<form id="userChangePasswordForm" class="form-horizontal"
		action="<c:url value="savePassword.xyz"/>" method="POST">
		<div class="row" style="margin-top: 10px; margin-bottom: 20px">
			<div class="col-md-12">
					<c:if test="${not empty error}"><i style="color: red;">${error}</i></c:if>
					<c:if test="${not empty success}"><i style="color: green;">${success}</i></c:if>
			</div>
		</div>

		<div class="form-group">
			<div class="col-md-3">
				<label class="control-label" for="userOldPassword">Old
					password</label>
			</div>
			<div class="col-md-5">
				<input required="required" type="password" class="form-control" name="userOldPassword" />
			</div>
			<div class="col-md-4"></div>
		</div>

		<div class="form-group">
			<div class="col-md-3">
				<label class="control-label" for="userNewPassword">New
					password</label>
			</div>
			<div class="col-md-5">
				<input required="required" type="password" class="form-control" name="userNewPassword" />
			</div>
			<div class="col-md-4"></div>
		</div>

		<div class="form-group">
			<div class="col-md-3">
				<label class="control-label" for="userConfirmNewPassword">Confirm
					new password</label>
			</div>
			<div class="col-md-5">
				<input required="required" type="password" class="form-control"
					name="userConfirmNewPassword" />
			</div>
			<div class="col-md-4"></div>
		</div>

		<div class="form-group">
			<div class="col-md-3"></div>
			<div class="col-md-5">
				<input type="submit" value="Update" class="btn btn-primary.gradient" />
				<input type="reset" value="Reset" class="btn btn-primary.gradient" />
			</div>
		</div>

	</form>
</body>
</html>