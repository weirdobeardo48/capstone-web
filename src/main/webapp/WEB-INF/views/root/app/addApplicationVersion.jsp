<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Add new application version</title>
<meta charset="utf-8">
<!-- Meta tags and CSS reference needed for bootstrap -->
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta name="viewport" content="width=device-width, initial-scale=1.0">

</head>
<body>
		<div class="row" style="margin-top: 10px; margin-bottom: 20px">
			<div class="col-md-12">
					<c:if test="${not empty error}"><i style="color: red;">${error}</i></c:if>
					<c:if test="${not empty success}"><i style="color: green;">${success}</i></c:if>
			</div>
		</div>
		<form id="registerForm" action="<c:url value="addApp.xyz"/>"
			method="POST" class="form-horizontal">
			<div class="row">
				
				<div class="col-md-3"><strong>Application version name</strong></div>
				<div class="col-md-5">
					<input required="required" type="text" class="form-control" placeholder="Enter application version name"
						name="inputAppVersionNumber" id="inputAppVersionNumber" value="<c:if test="${not empty napp}">${napp.appVersionNumber}</c:if>"autofocus />
				</div>
			</div>
			<br />
			<div class="form-group">
			<div class="col-md-3">
			<label class="control-label">Change log</label>
			</div>
			<div class="col-md-5">
			<textarea required="required" class="form-control" rows="5" name="inputChangeLog">${app.changeLog}</textarea>
			</div>
			</div>
			<br/>
			<div class="row">
				<div class="col-md-3"><strong>Application version link</strong></div>
				<div class="col-md-5">
					<input type="text" required="required" class="form-control" placeholder="Enter application version link"
						name="inputAppLink" id="inputAppLink" value="<c:if test="${not empty napp}">${napp.appLink}</c:if>"autofocus />
				</div>
			</div>
			<br/>
			<div class="row">
				<div class="col-md-3"></div>
				<div class="col-md-5">
					<input type="submit" value="Add"
						class="btn btn-primary.gradient" name="registerButton"
						id="registerButton" />
				</div>
				<div class="col-md-6"></div>
			</div>
			<br />
		</form>
</body>
</html>