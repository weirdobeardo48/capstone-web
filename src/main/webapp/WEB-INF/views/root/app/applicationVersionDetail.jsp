<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%--
  Created by IntelliJ IDEA.
  User: zLife
  Date: 28/9/2017
  Time: 9:42 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<html>
<head>
</head>
<body>
	<div class="row" style="margin-top: 10px; margin-bottom: 20px">
		<div class="col-md-12">
				<c:if test="${not empty error}"><i style="color: red;">${error}</i></c:if>
				<c:if test="${not empty success}"><i style="color: green;">${success}</i></c:if>
		</div>
	</div>
	<c:url value="/root/apps/${app.appVersionID}/save" var="requestUrl" />
	<form:form class="form-horizontal" action="${requestUrl}" method="post">
		<div class="form-group">
			<div class="col-md-3">
				<label class="control-label" >Application version ID</label>
			</div>
			<div class="col-md-5">
				<input class="form-control" required="required" name="appVersionID" id="appVersionID" value="${app.appVersionID}" readonly="true" />
			</div>
		</div>

		<div class="form-group">
			<div class="col-md-3">
				<label class="control-label">Application version number</label>
			</div>
			<div class="col-md-5">
				<input class="form-control" required="required" name="appVersionNumber" id="appVersionNumber"  value="${app.appVersionNumber}" readonly="true" />
			</div>
		</div>
		<div class="form-group">
		<div class="col-md-3">
			<label class="control-label">Change log</label>
		</div>
		<div class="col-md-5">
			<textarea class="form-control" required="required" rows="5" id="changeLog" name="changeLog">${app.changeLog}</textarea>
		</div>
		</div>

		<div class="form-group">
			<div class="col-md-3">
				<label class="control-label">Application version link</label>

			</div>
			<div class="col-md-5">
				<input class="form-control" required="required" name="appLink" value="${app.appLink}" />
			</div>
		</div>
		<div class="form-group">
			<div class="col-md-3"></div>
			<div class="col-md-5">
				<input type="submit" value="Update" class="btn btn-primary.gradient" />
				<input type="reset" value="Reset" class="btn btn-primary.gradient" />
			</div>
		</div>

	</form:form>
</body>
</html>
