<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">

<link rel="stylesheet" type="text/css"
	href='<c:url value="/resources/css/bootstrap.min.css" />' />
<link rel="stylesheet" type="text/css"
	href='<c:url value="/contents/css/style.css" />' />
<title>Application version management</title>
</head>
<body>
	<div class="row">
				<c:if test="${not empty error}"><i style="color: red;">${error}</i></c:if>
				<c:if test="${not empty success}"><i style="color: green;">${success}</i></c:if>
	</div>
	 <a href="<c:url value="addApp.xyz"/>" style="line-height: 60px; font-weight: 700">Add new application version</a>
		<table id="myTable" class="table table-striped" >  
		<thead>
			<tr>
				<th>Application version ID</th>
				<th>Application version name</th>
				<th>Edit</th>
			</tr>
		</thead>
		<tbody>
			<c:if test="${not empty appList}">
				<c:forEach items="${appList}" var="app">
					<tr>
						<td>${app.appVersionID}</td>
						<td>${app.appVersionNumber}</td>
						
						
						<td><a
							href="<c:url value="/root/apps/${app.appVersionID}-index.xyz"/>">Edit</a>
						</td>
					</tr>
				</c:forEach>
			</c:if>
		</tbody>
	</table>
	
	
</body>
</html>
<script>
$(document).ready(function() {
	$('a[data-confirm]').click(function(ev) {
		var href = $(this).attr('href');
		if (!$('#dataConfirmModal').length) {
			$('body').append('<div id="dataConfirmModal" class="modal" role="dialog" aria-labelledby="dataConfirmLabel" aria-hidden="true"><div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button><h3 id="dataConfirmLabel">Please Confirm</h3></div><div class="modal-body"></div><div class="modal-footer"><button class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button><a class="btn btn-primary" id="dataConfirmOK">OK</a></div></div>');
		} 
		$('#dataConfirmModal').find('.modal-body').text($(this).attr('data-confirm'));
		$('#dataConfirmOK').attr('href', href);
		$('#dataConfirmModal').modal({show:true});
		return false;
	});
});
</script>
<script>
$(document).ready(function() {
	 $('#myTable').dataTable();
});
</script>
