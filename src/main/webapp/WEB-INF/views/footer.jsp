<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<!-- Meta tags and CSS reference needed for bootstrap -->
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta name="viewport" content="width=device-width, initial-scale=1.0">

</head>
<body>
	<footer class="footer navbar-fixed-bottom">
	<div class="container">
		<div class="row">
			<div class="col-md-5">Copyright 2018 &copy; All rights reserved. Developed by T-Team</div>
			<div class="col-md-4"></div>
			<div class="col-md-3">
				<a href="mailto:contact@easyshoppingfeed.com?subject=Error Report"
					style="color: white">contact@cfml-contentfilter.com</a>
			</div>
		</div>
	</div>
	</footer>
</body>
</html>