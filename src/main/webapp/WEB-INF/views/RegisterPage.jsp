<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Register</title>
<meta charset="utf-8">
<!-- Meta tags and CSS reference needed for bootstrap -->
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta name="viewport" content="width=device-width, initial-scale=1.0">


</head>
<body>
	<jsp:include page="header.jsp" />
	<div class="container form-register">
		<hr width="100%"/>
		<h2>Register</h2>
		<form id="registerForm" action="<c:url value="/register.xyz"/>"
			method="POST" class="form-horizontal">
			<div class="row" style="margin-top: 10px; margin-bottom: 20px">
			<div class="col-md-12">
					<c:if test="${not empty error}"><i style="color: red;">${error}</i></c:if>
					<c:if test="${not empty success}"><i style="color: green;">${success}</i></c:if>
			</div>
		</div>
			<div class="row">
				
				<div class="col-md-4"><strong>Username</strong></div>
				<div class="col-md-8">
					<input type="text" class="form-control" placeholder="Enter username"
						name="inputUserName" required="required"  id="inputUserName" value="<c:if test="${not empty user}">${user.userName}</c:if>"autofocus />
				</div>
			</div>
			<br />
			<div class="row">
				<div class="col-md-4"><strong>Password</strong></div>
				<div class="col-md-8">
					<input type="password" class="form-control" placeholder="Enter password"
						name="inputPassword" required="required" id="inputPassword" value="<c:if test="${not empty user}">${user.userPassword}</c:if>" />
				</div>
			</div>
			<br />
			<!--Update 9/10/2017 12:18:03 AM -->
			<div class="row">
				<div class="col-md-4"><strong>Confirm password</strong></div>
				<div class="col-md-8">
					<input type="password" class="form-control" placeholder="Confirm password"
						name="inputConfirmPassword" required="required" id="inputConfirmPassword" value="<c:if test="${not empty confirmPassword}">${confirmPassword}</c:if>"/>
				</div>
			</div>
			<br />
			<!--  -->
			<div class="row">
				<div class="col-md-4"><strong>E-mail</strong></div>
				<div class="col-md-8">
					<input type="email" class="form-control" placeholder="Enter e-mail"
						name="inputEmail" required="required" id="inputEmail" value="<c:if test="${not empty user}">${user.userEmail}</c:if>" />
				</div>
			</div>
			<br />
			<div class="row">
				<div class="col-md-4"><strong>Phone number</strong></div>
				<div class="col-md-8">
					<input type="number" class="form-control"
						placeholder="Enter phone number" name="inputPhone"
						id="inputPhone" required="required"  value="<c:if test="${not empty user}">${user.userPhone}</c:if>"/>
				</div>
			</div>
			<br />
			<div class="row">
				<div class="col-md-4"></div>
				<div class="col-md-8">
					<input type="submit" value="Register"
						class="btn btn-primary.gradient" name="registerButton"
						id="registerButton"  style="width:100%; background-color:#2c90de; color: white"/>
				</div>
			</div>
			<br />
		</form>
	</div>
	<jsp:include page="footer.jsp" />
</body>
</html>