<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">

<link rel="stylesheet" type="text/css"
	href='<c:url value="/resources/css/bootstrap.min.css" />' />
<link rel="stylesheet" type="text/css"
	href='<c:url value="/contents/css/style.css" />' />
<link rel="stylesheet" type="text/css"
	href="font-awesome/css/font-awesome.min.css" />

<title>Moderator</title>
</head>
<body>
	<!-- HEADER -->
	<jsp:include page="../header.jsp" />
	<!-- 	CONTENT WILL BE SHOW HERE -->
	<div class="adminPage">
		<div class="row">
			<div class="col-md-3" align="left">
				<jsp:include page="adminSidebar.jsp" />
			</div>
			<div class="col-md-8" style="margin-left: 3%;">
				<div class="row">

					<c:choose>
					<c:when test="${null != userProfilePassword}">
							<h1 class="page-header">Change your password</h1>
							<jsp:include page="profile/adminProfilePassword.jsp" />
						</c:when>
					<c:when test="${not empty userProfile}">
							<h1 class="page-header">Update your profile</h1>
							<jsp:include page="profile/adminProfile.jsp" />
						</c:when>
						<c:when test="${not empty userPlan}">
							<h1 class="page-header">Add package for user</h1>
							<jsp:include page="user/adminUserPlan.jsp" />
						</c:when>
						<c:when test="${null !=  userListNav}">
							<h1 class="page-header">User management</h1>
							<jsp:include page="user/adminUserManagement.jsp" />
						</c:when>
						
						<c:when test="${not empty user}">
							<h1 class="page-header">User detail</h1>
							<jsp:include page="user/adminUserDetail.jsp" />
						</c:when>
						<c:when test="${null != listTransaction}">
							<h1 class="page-header">User transactions</h1>
							<jsp:include page="user/adminUserTransactionManagement.jsp" />
						</c:when>
						<c:when test="${not empty packListNav}">
							<h1 class="page-header">Package management</h1>
							<jsp:include page="pack/adminPackageManagement.jsp" />
						</c:when>
						
						<c:when test="${not empty pack}">
							<h1 class="page-header">Package detail</h1>
							<jsp:include page="pack/adminPackageDetail.jsp" />
						</c:when>
						<c:when test="${null !=  listTransactions}">
							<h1 class="page-header">Transaction management</h1>
							<jsp:include page="transaction/adminTransactionManagement.jsp" />
						</c:when>
						<c:when test="${not empty register}">
							<h1 class="page-header">Add new user</h1>
							<jsp:include page="user/adminAddUser.jsp" />
						</c:when>
					</c:choose>

				</div>
			</div>
		</div>
	</div>
	<!-- FOOTER -->
	<jsp:include page="../footer.jsp" />
</body>
</html>