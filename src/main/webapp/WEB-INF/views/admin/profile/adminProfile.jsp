<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" type="text/css"
	href='<c:url value="/resources/css/bootstrap.min.css" />' />
<link rel="stylesheet" type="text/css"
	href='<c:url value="/contents/css/style.css" />' />
<title>Moderator profile</title>
</head>
<body>
	<c:url value="/admin/profile/saveProfile.xyz" var="requestUrl" />
	<form:form class="form-horizontal" action="${requestUrl}" method="post"
		modelAttribute="puser">
		<div class="row" style="margin-top: 10px; margin-bottom: 20px">
		<div class="col-md-12">
				<c:if test="${not empty error}"><i style="color: red;">${error}</i></c:if>
				<c:if test="${not empty success}"><i style="color: green;">${success}</i></c:if>
		</div>
	</div>

		<div class="form-group">

			<div class="col-md-2">
				<form:label class="control-label" path="userPassword">Password</form:label>
			</div>
			<div class="col-md-5">
				<form:label class="control-label" path="userPassword">
					<a href="<c:url value="/admin/profile/adminProfilePassword.xyz"/>">Click
						here to change password</a>
				</form:label>
			</div>
			<div class="col-md-5"></div>
		</div>

		<div class="form-group">

			<div class="col-md-2">
				<form:label class="control-label" path="userEmail">User e-mail</form:label>

			</div>
			<div class="col-md-5">
				<form:input type="email" required="required" class="form-control" path="userEmail" />
			</div>
			<div class="col-md-5"></div>
		</div>

		<div class="form-group">

			<div class="col-md-2">
				<form:label class="control-label" path="userPhone">Phone Number</form:label>
			</div>
			<div class="col-md-5">
				<form:input required="required" class="form-control" path="userPhone" />
			</div>
			<div class="col-md-5"></div>
		</div>



		<div class="form-group">
			<div class="col-md-2"></div>
			<div class="col-md-5">
				<input type="submit" value="Update" class="btn btn-primary.gradient" />
				<input type="reset" value="Reset" class="btn btn-primary.gradient" />
			</div>
		</div>

	</form:form>
</body>
</html>