<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Transaction management</title>
</head>
<body>
	<a href="<c:url value="/admin/users/${hash}-plan.xyz"/>" style="line-height: 60px">
						<b>Add new transaction</b></a>
	<table id="myTable" class="table table-striped">
		<thead>
			<tr>
				<th>Transaction date</th>
				<th>Package name</th>
				<th>Package price</th>
				<th>Days of using</th>
			</tr>
		</thead>
		<tbody>
			<c:if test="${not empty listTransaction}">
				<c:forEach items="${listTransaction}" var="transaction">
					<tr>
						<td>${transaction.transactionnDate}</td>
						<td>${transaction.transactionPackageName}</td>
						<td><script>moneyconvert(${transaction.transactionPackagePrice})</script></td>
						<td>${transaction.transactionPackageDays}</td>
						
					</tr>
				</c:forEach>
			</c:if>
		</tbody>
	</table>
</body>
<script>
$(document).ready(function() {
	 $('#myTable').dataTable();
});
</script>
</html>
