<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Add new user</title>
<meta charset="utf-8">
<!-- Meta tags and CSS reference needed for bootstrap -->
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta name="viewport" content="width=device-width, initial-scale=1.0">

</head>
<body>
		<div class="row" style="margin-top: 10px; margin-bottom: 20px">
			<div class="col-md-12">
					<c:if test="${not empty error}"><i style="color: red;">${error}</i></c:if>
					<c:if test="${not empty success}"><i style="color: green;">${success}</i></c:if>
			</div>
		</div>
		<form id="registerForm" action="<c:url value="adduser.xyz"/>"
			method="POST" class="form-horizontal">
			<div class="row">
				<div class="col-md-2"><strong>Username</strong></div>
				<div class="col-md-4">
					<input required="required" type="text" class="form-control" placeholder="Enter username"
						name="inputUserName" id="inputUserName" value="<c:if test="${not empty nuser}">${nuser.userName}</c:if>"autofocus />
				</div>
				<div class="col-md-6"></div>
			</div>
			<br />
			<div class="row">
				<div class="col-md-2"><strong>Password</strong></div>
				<div class="col-md-4">
					<input required="required" type="password" class="form-control" placeholder="Enter password"
						name="inputPassword" id="inputPassword" value="<c:if test="${not empty nuser}">${nuser.userPassword}</c:if>" />
				</div>
				<div class="col-md-6"></div>
			</div>
			<br />
			<!--Update 9/10/2017 12:18:03 AM -->
			<div class="row">
				<div class="col-md-2"><strong>Confirm password</strong></div>
				<div class="col-md-4">
					<input required="required" type="password" class="form-control" placeholder="Confirm password"
						name="inputConfirmPassword" id="inputConfirmPassword" value="<c:if test="${not empty confirmPassword}">${confirmPassword}</c:if>"/>
				</div>
				<div class="col-md-6"></div>
			</div>
			<br />
			<!--  -->
			<div class="row">
				<div class="col-md-2"><strong>E-mail</strong></div>
				<div class="col-md-4">
					<input required="required" type="email" class="form-control" placeholder="Enter e-mail"
						name="inputEmail" id="inputEmail" value="<c:if test="${not empty nuser}">${nuser.userEmail}</c:if>" />
				</div>
				<div class="col-md-6"></div>
			</div>
			<br />
			<div class="row">
				<div class="col-md-2"><strong>Phone number</strong></div>
				<div class="col-md-4">
					<input required="required" type="number" class="form-control"
						placeholder="Enter phone number" name="inputPhone"
						id="inputPhone" value="<c:if test="${not empty nuser}">${nuser.userPhone}</c:if>"/>
				</div>
				<div class="col-md-6"></div>
			</div>
			<br />
			<div class="row">
				<div class="col-md-2"></div>
				<div class="col-md-4">
					<input type="submit" value="Add"
						class="btn btn-primary.gradient" name="registerButton"
						id="registerButton" />
				</div>
				<div class="col-md-6"></div>
			</div>
			<br />
		</form>
</body>
<script>
$(function () {
	  $('[data-toggle="tooltip"]').tooltip()
	})
</script>
</html>