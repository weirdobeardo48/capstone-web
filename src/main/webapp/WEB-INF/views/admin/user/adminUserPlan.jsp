<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" type="text/css"
	href='<c:url value="/resources/css/bootstrap.min.css" />' />
<link rel="stylesheet" type="text/css"
	href='<c:url value="/contents/css/style.css" />' />
<title>Add new transaction</title>
</head>
<body>
	<c:url value="/admin/users/savePlan.xyz" var="requestUrl" />
	<form:form class="form-horizontal" action="${requestUrl}" method="post"
		modelAttribute="user" style="margin-top: 30px">
		<input class="form-control" type="hidden" name="userID" id="userID" value="${user.getUserID()}" />
		<div class="form-group">
			<div class="col-md-3">
				<label class="control-label">Package</label>
			</div>
			<div class="col-md-5">
				<select class="form-control" name="packages" id="packages">
					<option value="-1">Choose package</option>
					<c:forEach items="${packs}" var="packs">
						<option value="${packs.packageID}">${packs.packageName}</option>
					</c:forEach>
				</select>
			</div>
		</div>

		<div class="form-group">
			<div class="col-md-3"></div>
			<div class="col-md-5">
				<input type="submit" value="Update" class="btn btn-primary.gradient" />
			</div>
		</div>

	</form:form>
</body>
</html>