<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>User management</title>
</head>
<body>
	
					<c:if test="${not empty error}">
					<div class="row" style="margin-top: 10px; margin-bottom: 20px">
							<div class="col-md-12">
								<i style="color: red;">${error}</i>
							</div>
					</div></c:if>
					<c:if test="${not empty success}">
					<div class="row" style="margin-top: 10px; margin-bottom: 20px">
							<div class="col-md-12">
								<i style="color: green;">${success}</i>
							</div>
					</div></c:if>
			
	 <a href="<c:url value="adduser.xyz"/>" style="line-height: 60px"><b>Add new user</b></a>
     <table id="myTable" class="table table-striped">  
        <thead>
			<tr>
				<th>Username</th>
				<th>User e-mail</th>
				<th>Expired days</th>
				<th>View transactions</th>
				<th>User status</th>
				<th>Edit</th>
			</tr>
		</thead>
		<tbody>
			<c:if test="${not empty userList}">
				<c:forEach items="${userList}" var="user">
					<tr>
						<td>${user.userName}</td>
						<td>${user.userEmail}</td>
						
							
						<td>${user.getSubscriptionTable().getExpiredDate()}</td>
							
						<td><a href="<c:url value="/admin/users/${user.hashCode()}tran-index.xyz"/>">View</a>
						</td>
						<td><c:choose>
								<c:when test="${user.isActive == 1}">
									<a href="<c:url value="/admin/users/${user.hashCode()}-disable"/>" onclick="return confirm('Do you want to disable this user?');">Enable</a>
								</c:when>
								<c:otherwise>
									<a href="<c:url value="/admin/users/${user.hashCode()}-enable"/>" onclick="return confirm('Do you want to enable this user?');">Disable</a>
									
								</c:otherwise>
							</c:choose>
							</td>
						<td><a
							href="<c:url value="/admin/users/${user.hashCode()}-index.xyz"/>">Edit</a>
						</td>
					</tr>
				</c:forEach>
			</c:if>
		</tbody>
	</table>
	
	
</body>
</html>
<script>
$(document).ready(function() {
	 $('#myTable').dataTable();
	$('a[data-confirm]').click(function(ev) {
		var href = $(this).attr('href');
		if (!$('#dataConfirmModal').length) {
			$('body').append('<div id="dataConfirmModal" class="modal" role="dialog" aria-labelledby="dataConfirmLabel" aria-hidden="true"><div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button><h3 id="dataConfirmLabel">Please Confirm</h3></div><div class="modal-body"></div><div class="modal-footer"><button class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button><a class="btn btn-primary" id="dataConfirmOK">OK</a></div></div>');
		} 
		$('#dataConfirmModal').find('.modal-body').text($(this).attr('data-confirm'));
		$('#dataConfirmOK').attr('href', href);
		$('#dataConfirmModal').modal({show:true});
		return false;
	});
	
	
});
</script>
