<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">

</head>
<body>

	<div class="sidebar-nav-fixed" style="margin-top: -70px">
	<div style="height: 25vh; margin-top: 50px;color: white; padding-top: 10%; padding-left: 10%" class="sidebar-header">
		<h2>${sessionScope.currentUser.userName}</h2>
		<h4>${sessionScope.currentUser.userEmail}</h4>
	</div>
		<div class="well" style="height: 100%;">
			<ul class="nav">
<!-- 				<li class="nav-header">Admin</li> -->
				<c:choose>
				<c:when test="${not empty userPlan || null !=  userListNav || not empty user || null != listTransaction || not empty addtrans || not empty register}">
				<li class="active">
				<a href="<c:url value="/admin/users"/>"> <i
						class="fa fa-user fa-fw" aria-hidden="true"></i> User management
				</a></li>
				</c:when>
				<c:otherwise>
				<li>
				<a href="<c:url value="/admin/users"/>"> <i
						class="fa fa-user fa-fw" aria-hidden="true"></i> User management
				</a></li>
				</c:otherwise>
				</c:choose>
				<c:choose>
				<c:when test="${not empty packListNav || not empty pack}">
				<li class="active"><a href="<c:url value="/admin/packs"/>"> <i
						class="fa fa-home fa-fw " aria-hidden="true"></i> Package management
				</a></li>
				</c:when>
				<c:otherwise>
				<li><a href="<c:url value="/admin/packs"/>"> <i
						class="fa fa-home fa-fw " aria-hidden="true"></i> Package management
				</a></li>
				</c:otherwise>
				</c:choose>
				<c:choose>
				<c:when test="${null !=  listTransactions}">
				<li class="active"><a href="<c:url value="/admin/transactions"/>"> <i
						class="fa fa-home fa-fw " aria-hidden="true"></i> Transaction management
				</a></li>
				</c:when>
				<c:otherwise>
				<li><a href="<c:url value="/admin/transactions"/>"> <i
						class="fa fa-home fa-fw " aria-hidden="true"></i> Transaction management
				</a></li>
				</c:otherwise>
				</c:choose>
				<c:choose>
				<c:when test="${null != userProfilePassword || not empty userProfile}">
				<li class="active"><a href="<c:url value="/admin/profiles"/>"> <i
						class="fa fa-user fa-fw" aria-hidden="true"></i> Profile
				</a></li>
				</c:when>
				<c:otherwise>
				<li><a href="<c:url value="/admin/profiles"/>"> <i
						class="fa fa-user fa-fw" aria-hidden="true"></i> Profile
				</a></li>
				</c:otherwise>
				</c:choose>
				
				
			</ul>
		</div>
		<!--/.well -->
	</div>
	<!--/sidebar-nav-fixed -->
	
</body>
</html>