<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<html>
<head>

<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">

<link rel="stylesheet" type="text/css"
	href='<c:url value="/resources/css/bootstrap.min.css" />' />
<link rel="stylesheet" type="text/css"
	href='<c:url value="/contents/css/style.css" />' />
<title>Package management</title>
</head>
<body>
	<div style="height: 30px"></div>
	<table id="myTable" class="table table-striped"> 
		<thead>
			<tr>
				<th>Package name</th>
				<th>Days of using</th>
				<th>Package price</th>
				<th>Edit</th>
			</tr>
		</thead>
		<tbody>
			<c:if test="${not empty packList}">
				<c:forEach items="${packList}" var="pack">
					<tr>
						<td>${pack.packageName}</td>
						<td>${pack.daysOfUsing}</td>
						<td><script>moneyconvert(${pack.packagePrice});</script></td>
						<td><a
							href="<c:url value="/admin/packs/${pack.hashCode()}-index.xyz"/>">Edit</a>
						</td>
					</tr>
				</c:forEach>
			</c:if>
		</tbody>
	</table>
	
</body>
<script>
$(document).ready(function() {
	 $('#myTable').dataTable();
});
</script>
</html>
