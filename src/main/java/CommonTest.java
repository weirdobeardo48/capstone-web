

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Test;

import capstone.common.Common;
import capstone.model.UserTable;

public class CommonTest {
	ArrayList<UserTable> list = new ArrayList<UserTable>() {{
		add(new UserTable(1,"user1","079E76C0A5ECFEA075780EAEF8A9DD4F","user1@gmail.com","0912234567",1));
		add(new UserTable(2,"user2","079E76C0A5ECFEA075780EAEF8A9DD4F","user2@gmail.com","0912234568",1));
		add(new UserTable(3,"user3","079E76C0A5ECFEA075780EAEF8A9DD4F","user3@gmail.com","0912234562",1));
		add(new UserTable(4,"admin","079E76C0A5ECFEA075780EAEF8A9DD4F","admin@gmail.com","0912234561",0));
		add(new UserTable(5,"mod","079E76C0A5ECFEA075780EAEF8A9DD4F","mod@gmail.com","0912234563",2));
	}};
	UserTable u = new UserTable();
	@Test
	public void testCheckExistUserSuccess() {
		assertNotNull(Common.INSTANCE.checkExistUser(list, "user1", "079E76C0A5ECFEA075780EAEF8A9DD4F"));
	}
	
	@Test
	public void testCheckExistUserFail() {
		assertNull(Common.INSTANCE.checkExistUser(list, "user1", "T-team123"));
	}

	@Test
	public void testCheckToResetFail() {
		assertNull(Common.INSTANCE.checkToReset(list, "user1", "user2@gmail.com"));
	}
	
	@Test
	public void testCheckToResetSuccess() {
		assertNotNull(Common.INSTANCE.checkToReset(list, "user1", "user1@gmail.com"));
	}

	
	@Test
	public void testCheckExistUsernameFail() {
		u.setUserName("user1");
		assertFalse(Common.INSTANCE.checkExistUsername(list, u));
	}
	
	@Test
	public void testCheckExistUsernameSuccess() {
		u.setUserName("user4");
		assertTrue(Common.INSTANCE.checkExistUsername(list, u));
	}
	

	@Test
	public void testCheckExistEmailFail() {
		u.setUserEmail("user1@gmail.com");
		assertFalse(Common.INSTANCE.checkExistEmail(list, u));
	}

	@Test
	public void testCheckExistEmailSuccess() {
		u.setUserEmail("user4@gmail.com");
		assertTrue(Common.INSTANCE.checkExistEmail(list, u));
	}

	@Test
	public void testCheckExistPhoneFail() {
		u.setUserPhone("0912234568");
		assertFalse(Common.INSTANCE.checkExistPhone(list, u));
	}
	
	@Test
	public void testCheckExistPhoneSuccess() {
		u.setUserPhone("0922234568");
		assertTrue(Common.INSTANCE.checkExistPhone(list, u));
	}

	@Test
	public void testCheckEmailForUpdateFail() {
		u.setUserID(1);
		u.setUserEmail("user2@gmail.com");
		assertFalse(Common.INSTANCE.checkEmailForUpdate(list, u));
	}
	
	@Test
	public void testCheckEmailForUpdateSuccess() {
		u.setUserID(1);
		u.setUserEmail("user1@gmail.com");
		assertTrue(Common.INSTANCE.checkEmailForUpdate(list, u));
	}

	@Test
	public void testCheckPhoneForUpdateSuccess() {
		u.setUserID(1);
		u.setUserPhone("0912234567");
		assertTrue(Common.INSTANCE.checkPhoneForUpdate(list, u));
	}
	
	@Test
	public void testCheckPhoneForUpdateFail() {
		u.setUserID(1);
		u.setUserPhone("0912234568");
		assertFalse(Common.INSTANCE.checkPhoneForUpdate(list, u));
	}

	@Test
	public void testCheckEmptyFail() {
		assertFalse(Common.INSTANCE.checkEmpty("   "));
	}
	
	@Test
	public void testEmptySuccess() {
		assertTrue(Common.INSTANCE.checkEmpty("Hello"));
	}

	@Test
	public void testCheckMinLengthFail() {
		assertFalse(Common.INSTANCE.checkMinLength("aaa"));
	}
	
	@Test
	public void testCheckMinLengthSuccess() {
		assertTrue(Common.INSTANCE.checkMinLength("aaaaaaaaa"));
	}
	
	@Test
	public void testCheckMaxLengthFail() {
		assertFalse(Common.INSTANCE.checkMaxLength("aaaaaaaaaaaaaaaaaaaaaaa"));
	}
	
	@Test
	public void testCheckMaxLengthSuccess() {
		assertTrue(Common.INSTANCE.checkMaxLength("aaaaaaaaa"));
	}

	@Test
	public void testCheckValidUsernameFail() {
		assertFalse(Common.INSTANCE.checkValidUsername("user 1@#%"));
	}
	
	@Test
	public void testCheckValidUsernameSuccess() {
		assertTrue(Common.INSTANCE.checkValidUsername("truongnxvip"));
	}

	@Test
	public void testCheckValidEmailFail() {
		assertFalse(Common.INSTANCE.checkValidEmail("user"));
	}

	@Test
	public void testCheckValidEmailSuccess() {
		assertTrue(Common.INSTANCE.checkValidEmail("hello@gmail.com"));
	}
	
	@Test
	public void testCheckValidPasswordFail() {
		assertFalse(Common.INSTANCE.checkValidPassword("hoalac@123"));
	}
	
	@Test
	public void testCheckValidPasswordSuccess() {
		assertTrue(Common.INSTANCE.checkValidPassword("Hoalac@123"));
	}

	@Test
	public void testCheckValidPhoneFail() {
		assertFalse(Common.INSTANCE.checkValidPhone("12356"));
	}
	
	@Test
	public void testCheckValidPhoneSuccess() {
		assertTrue(Common.INSTANCE.checkValidPhone("0912283456"));
	}

}
