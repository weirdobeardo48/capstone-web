package capstone.dao.interfaces;

import capstone.dao.AbstractDao;
import capstone.dao.EntityDao;
import capstone.model.UserTable;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository("usertableDao")
public class UserTableDaoImpl extends AbstractDao<Integer, UserTable> implements EntityDao<UserTable> {
  
    public UserTable findById(int id) {
        return getByKey(id);
    }

    public void save(UserTable entity) {
        persist(entity);
    }

    public void update(UserTable entity) {
        merge(entity);
    }

    public void remove(UserTable entity) {
        delete(entity);
    }

    public List<UserTable> getList() {
        return getSession().createQuery("from UserTable", UserTable.class).getResultList();
    }
}
