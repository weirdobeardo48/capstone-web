package capstone.dao.interfaces;

import capstone.dao.AbstractDao;
import capstone.dao.EntityDao;
import capstone.model.PackageTable;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository("packagetableDao")
public class PackageTableDaoImpl extends AbstractDao<Integer, PackageTable> implements EntityDao<PackageTable> {
  
    public PackageTable findById(int id) {
        return getByKey(id);
    }

    public void save(PackageTable entity) {
        persist(entity);
    }

    public void update(PackageTable entity) {
        merge(entity);
    }

    public void remove(PackageTable entity) {
        delete(entity);
    }

    public List<PackageTable> getList() {
        return getSession().createQuery("from PackageTable", PackageTable.class).getResultList();
    }
}
