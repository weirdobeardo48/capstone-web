package capstone.dao.interfaces;

import capstone.dao.AbstractDao;
import capstone.dao.EntityDao;
import capstone.model.SubscriptionTable;
import capstone.model.UserTable;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository("subscriptiontableDao")
public class SubscriptionTableDaoImpl extends AbstractDao<Integer, SubscriptionTable>
        implements EntityDao<SubscriptionTable> {

    public SubscriptionTable findById(int id) {
        return getByKey(id);
    }

    public void save(SubscriptionTable entity) {
        persist(entity);
    }

    public void update(SubscriptionTable entity) {
        merge(entity);
    }

   
    public void remove(SubscriptionTable entity) {
        delete(entity);
    }

   
    public List<SubscriptionTable> getList() {
        return getSession().createQuery("from  SubscriptionTable ", SubscriptionTable.class).getResultList();
    }

    public SubscriptionTable getByUser(UserTable user){
        return getSession().createQuery("from SubscriptionTable s where s.usertableByUserId = :user",
        		SubscriptionTable.class)
                .setParameter("user", user)
                .getSingleResult();
    }
}
