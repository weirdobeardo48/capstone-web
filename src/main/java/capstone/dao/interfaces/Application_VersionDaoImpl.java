package capstone.dao.interfaces;

import java.util.List;

import org.springframework.stereotype.Repository;

import capstone.dao.AbstractDao;
import capstone.dao.EntityDao;
import capstone.model.Application_Version;

@Repository("applicationversionDao")
public class Application_VersionDaoImpl extends AbstractDao<Integer, Application_Version> implements EntityDao<Application_Version> {
  
    public Application_Version findById(int id) {
        return getByKey(id);
    }

    public void save(Application_Version entity) {
        persist(entity);
    }

    public void update(Application_Version entity) {
        merge(entity);
    }

    public void remove(Application_Version entity) {
        delete(entity);
    }

    public List<Application_Version> getList() {
        return getSession().createQuery("from Application_Version", Application_Version.class).getResultList();
    }
}
