package capstone.dao.interfaces;

import capstone.dao.AbstractDao;
import capstone.dao.EntityDao;
import capstone.model.TransactionTable;
import capstone.model.UserTable;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository("transactiontableDao")
public class TransactionTableDaoImpl extends AbstractDao<Integer, TransactionTable>
        implements EntityDao<TransactionTable> {

    public TransactionTable findById(int id) {
        return getByKey(id);
    }

    public void save(TransactionTable entity) {
        persist(entity);
    }

    public void update(TransactionTable entity) {
        merge(entity);
    }

   
    public void remove(TransactionTable entity) {
        delete(entity);
    }

   
    public List<TransactionTable> getList() {
        return getSession().createQuery("from  TransactionTable ", TransactionTable.class).getResultList();
    }

    public TransactionTable getByUser(UserTable user){
        return getSession().createQuery("from TransactionTable s where s.usertableByUserId = :user",
        		TransactionTable.class)
                .setParameter("user", user)
                .getSingleResult();
    }
}
