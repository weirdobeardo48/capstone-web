package capstone.dao;

import java.util.List;



public interface EntityDao<T> {
    T findById(int id);

    void save(T entity);

    void update(T entity);

    void remove(T entity);

    List<T> getList();
}
