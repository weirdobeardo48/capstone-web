package capstone.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;

@Controller
@RequestMapping("/logout")
public class LogoutController {

	@RequestMapping(method = RequestMethod.GET)
	public String requestlogout(HttpServletRequest request) {
		if(request.getSession() != null){
			request.getSession().invalidate();
		}
		return "redirect:/home";
	}
}
