package capstone.controller;

import capstone.common.Common;
import capstone.common.Common_const;
import capstone.model.SubscriptionTable;
import capstone.model.UserTable;
import capstone.service.EntityService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.annotation.Resource;
import java.util.List;

@Controller
public class RegisterController {

    @Resource(name = "usertableService")
    private EntityService<UserTable> userService;
    @Resource(name = "subscriptiontableService")
    private EntityService<SubscriptionTable> subService;

    @RequestMapping(value = {"/register", "register"}, method = RequestMethod.GET)
    public String redirectRegisterRequest() {
        return "redirect:/register.xyz";
    }

    @RequestMapping(value = "register.xyz", method = RequestMethod.GET)
    public String register() {
        return "RegisterPage";
    }

    @RequestMapping(value = "register.xyz", method = RequestMethod.POST)
    public String addNewUser(Model model, @RequestParam(name = "inputUserName") String username,
                             @RequestParam(name = "inputPassword") String password,
                             @RequestParam(name = "inputConfirmPassword", defaultValue = "") String confirmPassword,
                             @RequestParam(name = "inputEmail") String email,
                             @RequestParam(name = "inputPhone") String phone,
                             final RedirectAttributes redirectAttributes) {
        UserTable user = new UserTable();
        user.setUserPhone(phone);
        user.setUserEmail(email);
        user.setUserName(username);
        user.setUserPassword(password);
        user.setIsActive((byte)1);
        user.setUserType(Common_const.isUSER);
        model.addAttribute("user", user);
        //---Update 9/10/2017 12:18:03 AM
        //---
        List<UserTable> users = userService.getList();

        if (!Common.INSTANCE.checkEmpty(username)
                || !Common.INSTANCE.checkEmpty(password)
                || !Common.INSTANCE.checkEmpty(email)
                || !Common.INSTANCE.checkEmpty(phone)) {
            model.addAttribute("error", "You have to fill all the required fields!");
            return "RegisterPage";
        }
        if (!Common.INSTANCE.checkMinLength(username) || !Common.INSTANCE.checkMinLength(password)) {
            model.addAttribute("error", "Your username and password must be at least 8 characters!");
            return "RegisterPage";
        }
        
        if (!Common.INSTANCE.checkMaxLength(username) || !Common.INSTANCE.checkMaxLength(password)) {
            model.addAttribute("error", "Your username and password must be at most 16 characters!");
            return "RegisterPage";
        }
        
        if (!Common.INSTANCE.checkValidUsername(username)) {
            model.addAttribute("error", "Your username must contain only letters and numbers!");
            return "RegisterPage";
        }

        if (!Common.INSTANCE.checkExistUsername(users, user)) {
            model.addAttribute("error", "This username has already existed!");
            return "RegisterPage";
        }
        
        //---Update 9/10/2017 12:18:03 AM
        if (!Common.INSTANCE.checkValidPassword(password)) {
            model.addAttribute("error", "Your password must contain at least one lower character and one upper character!");
            return "RegisterPage";
        }
        
        if (!confirmPassword.equals(password)) {
            model.addAttribute("confirmPassword", confirmPassword);
            model.addAttribute("error", "Your confirm password did not match!");
            return "RegisterPage";
        }
        //---
        if (!Common.INSTANCE.checkValidEmail(email)) {
            model.addAttribute("error", "Your e-mail is not valid!");
            return "RegisterPage";
        }
        if (!Common.INSTANCE.checkExistEmail(users, user)) {
            model.addAttribute("error", "This email has been used!");
            return "RegisterPage";
        }
        
        if (!Common.INSTANCE.checkValidPhone(phone)) {
            model.addAttribute("error", "Your phone number is not valid!");
            return "RegisterPage";
        }
        
        if (!Common.INSTANCE.checkExistPhone(users, user)) {
            model.addAttribute("error", "This phone number has been used!");
            return "RegisterPage";
        }
        SubscriptionTable subscriptiontableEntity = new SubscriptionTable();
        try {
            password = Common.INSTANCE.getHashMD5(password);
        }
        catch(Exception e) {}
        user.setUserPassword(password);
        userService.save(user);
        users = userService.getList();
        UserTable us = users.stream().filter(u -> u.getUserName().equals(username)).findFirst().orElseThrow(null);
        subscriptiontableEntity
                .setUserTable(us);

        subService.save(subscriptiontableEntity);
        String content = "Hello "+username+", your account is created on CFML-ContentFilter. Enjoy it!";
        String subject = "Register Successful";
        Common.INSTANCE.sendEmail(user.getUserEmail(), subject, content);
        redirectAttributes.addFlashAttribute("user", user);	
        model.addAttribute("success", "Register Successful!");
        return "LoginPage";

    }
}