package capstone.controller;

import capstone.common.Common;
import capstone.model.SubscriptionTable;
import capstone.model.TransactionTable;
import capstone.model.Application_Version;
import capstone.model.PackageTable;
import capstone.model.UserTable;
import capstone.service.EntityService;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import java.security.NoSuchAlgorithmException;
import java.text.ParseException;
import java.util.List;
import java.util.stream.Collectors;

@Controller
@RequestMapping("/root")
public class RootController {
	
    @Resource(name = "usertableService")
    private EntityService<UserTable> userService;
    @Resource(name = "transactiontableService")
    private EntityService<TransactionTable> transactionService;
    @Resource(name = "subscriptiontableService")
    private EntityService<SubscriptionTable> subService;
    @Resource(name = "packagetableService")
    private EntityService<PackageTable> packageService;
    @Resource(name = "applicationversionService")
    private EntityService<Application_Version> appService;
    @Resource(name = "transactiontableService")
    private EntityService<TransactionTable> transService;

    
    private boolean isRoot(HttpServletRequest request) {
        if (request.getSession().getAttribute("currentUser") == null)
            return false;
        UserTable user = (UserTable) request.getSession().getAttribute("currentUser");
        return Common.INSTANCE.isRoot(user);
    }

    // display user profile
    @RequestMapping(value = "/profs", method = RequestMethod.GET)
    public String redirectRootProfile() {
        return "redirect:/root/profs/index.xyz";
    }

    @RequestMapping(value = {"/profs/index.xyz"}, method = RequestMethod.GET)
    public String editRootProfile(Model model, HttpServletRequest request) {
        if (!isRoot(request))
            return "redirect:/index.xyz";
        UserTable user = (UserTable) request.getSession().getAttribute("currentUser");
        model.addAttribute("puser", user);
        model.addAttribute("userProfile", "userProfile");
        return "root/RootPage";
    }
 
    
    @RequestMapping(value = {"/profs/rootProfilePassword.xyz"}, method = RequestMethod.GET)
    public String editRootPassword(Model model, HttpServletRequest request) {
        if (!isRoot(request))
            return "redirect:/index.xyz";
        UserTable user = (UserTable) request.getSession().getAttribute("currentUser");
        model.addAttribute("puser", user);
        model.addAttribute("userProfilePassword", "userProfilePassword");
        return "root/RootPage";
    }

    // save change of user profile
    @RequestMapping(value = {"/profs/saveProfile.xyz"}, method = RequestMethod.POST)
    public String updateRootProfile(@ModelAttribute UserTable puser, Model model, HttpServletRequest request) {
        if (!isRoot(request))
            return "redirect:/index.xyz";
        UserTable currentUser = (UserTable) request.getSession().getAttribute("currentUser");

        List<UserTable> users = userService.getList();
        UserTable oldUser = null;
        for (UserTable usertableEntity : users) {
            if (usertableEntity.getUserName().equals(currentUser.getUserName())) {
                oldUser = usertableEntity;
                break;
            }
        }

        if (oldUser != null) {
        	puser.setUserID(oldUser.getUserID());
            if (!Common.INSTANCE.checkEmpty(puser.getUserEmail())
                    || !Common.INSTANCE.checkEmpty(puser.getUserPhone())){
                model.addAttribute("error", "You have to fill all the required fields!");
                model.addAttribute("puser", puser);
                model.addAttribute("userProfile", "userProfile");
                return "root/RootPage";
            }
            oldUser.setUserEmail(puser.getUserEmail());
            oldUser.setUserPhone(puser.getUserPhone());
            if (!Common.INSTANCE.checkValidEmail(puser.getUserEmail())) {
                // currentUser.setUserEmail(oldUser.getUserEmail());
                model.addAttribute("error", "Your e-mail is not valid!");
                model.addAttribute("puser", puser);
                model.addAttribute("userProfile", "userProfile");
                return "root/RootPage";
            }
            if (!Common.INSTANCE.checkValidPhone(puser.getUserPhone())) {
                // currentUser.setUserPhoneNo(oldUser.getUserPhoneNo());
                model.addAttribute("error", "Your phone number is not valid!");
                model.addAttribute("puser", puser);
                model.addAttribute("userProfile", "userProfile");
                return "root/RootPage";
            }
            if (!Common.INSTANCE.checkEmailForUpdate(users, oldUser)) {
                // currentUser.setUserAddress(oldUser.getUserAddress());
                model.addAttribute("error", "This email has been used!");
                model.addAttribute("puser", puser);
                model.addAttribute("userProfile", "userProfile");
                return "root/RootPage";
            }
            
            if (!Common.INSTANCE.checkPhoneForUpdate(users, oldUser)) {
                // currentUser.setUserAddress(oldUser.getUserAddress());
                model.addAttribute("error", "This phone number has been used!");
                model.addAttribute("puser", puser);
                model.addAttribute("userProfile", "userProfile");
                return "root/RootPage";
            }
            currentUser.setUserEmail(puser.getUserEmail());
            currentUser.setUserPhone(puser.getUserPhone());
            userService.update(oldUser);
            model.addAttribute("puser", puser);
            model.addAttribute("userProfile", "userProfile");
            model.addAttribute("success", "Your profile is updated successfully!");
            return "root/RootPage";
        }
        model.addAttribute("puser", puser);
        model.addAttribute("rootProfile", "rootProfile");
        return "root/RootPage";
    }

    // save change of user password
    @RequestMapping(value = {"/profs/savePassword.xyz"}, method = RequestMethod.POST)
    public String updateRootPassword(Model model, HttpServletRequest request,
                                     @RequestParam(required = false, name = "userOldPassword") String oldPassword,
                                     @RequestParam(required = false, name = "userNewPassword") String newPassword,
                                     @RequestParam(required = false, name = "userConfirmNewPassword") String confirmNewPassword) throws NoSuchAlgorithmException {
        
    	if (!isRoot(request))
            return "redirect:/index.xyz";
        if (oldPassword == null || newPassword == null || confirmNewPassword == null) {
            model.addAttribute("userProfilePassword", "userProfilePassword");
            model.addAttribute("error", "You have to fill all the required fields. ");
            return "root/RootPage";
        }
       

        UserTable currentUser = (UserTable) request.getSession().getAttribute("currentUser");

        List<UserTable> users = userService.getList();
        UserTable oldUser = null;
        for (UserTable usertableEntity : users) {
            if (usertableEntity.getUserName().equals(currentUser.getUserName())) {
                oldUser = usertableEntity;
                break;
            }
        }

        if (oldUser != null) {
        	
            if (oldPassword.isEmpty() || newPassword.isEmpty() || confirmNewPassword.isEmpty()) {
                model.addAttribute("error", "You have to fill all the required fields. ");
                model.addAttribute("user", oldUser);
                model.addAttribute("userProfilePassword", "userProfilePassword");
                return "root/RootPage";
            }
            
            oldUser.setUserPassword(newPassword);
            if (!Common.INSTANCE.getHashMD5(oldPassword).equals(oldUser.getUserPassword())) {
                model.addAttribute("error", "Your password is wrong!");
                model.addAttribute("user", oldUser);
                model.addAttribute("userProfilePassword", "userProfilePassword");
                return "root/RootPage";
            }
            if (oldPassword.equals(newPassword)) {
                model.addAttribute("error", "New password must be different from old password!");
                model.addAttribute("user", oldUser);
                model.addAttribute("userProfilePassword", "userProfilePassword");
                return "root/RootPage";
            }
            
            if (!newPassword.equals(confirmNewPassword)) {
                model.addAttribute("error", "Password does not match the confirm password!");
                model.addAttribute("user", oldUser);
                model.addAttribute("userProfilePassword", "userProfilePassword");
                return "root/RootPage";
            }

            if (!Common.INSTANCE.checkMinLength(newPassword)) {
                model.addAttribute("error", "Your password must be at least 8 characters!");
                model.addAttribute("user", oldUser);
                model.addAttribute("userProfilePassword", "userProfilePassword");
                return "root/RootPage";
            }
            
            if (!Common.INSTANCE.checkMaxLength(newPassword)) {
                model.addAttribute("error", "Your password must be at most 16 characters!");
                model.addAttribute("user", oldUser);
                model.addAttribute("userProfilePassword", "userProfilePassword");
                return "root/RootPage";
            }
            
            if (!Common.INSTANCE.checkValidPassword(newPassword)) {
                model.addAttribute("error", "Your password must contain at least a lower and an upper character!");
                model.addAttribute("user", oldUser);
                model.addAttribute("userProfilePassword", "userProfilePassword");
                return "root/RootPage";
            }
            
            oldUser.setUserPassword(Common.INSTANCE.getHashMD5(newPassword));
            currentUser.setUserPassword(Common.INSTANCE.getHashMD5(newPassword));
            userService.update(oldUser);

            model.addAttribute("user", oldUser);
            model.addAttribute("userProfilePassword", "userProfilePassword");
            model.addAttribute("success", "Your password is updated successfully!");
            return "root/RootPage";
        }

        model.addAttribute("user", currentUser);
        model.addAttribute("userProfilePassword", "userProfilePassword");
        return "root/RootPage";
        
    }

    
    
    // ## ==================== USER MANAGEMENT ==================== ##

    @RequestMapping(value = {"", "/", ".xyz", "/index", "/index.xyz", "/users"}, method = RequestMethod.GET)
    public String redirectUserRequest() {
    	return "redirect:/root/users/index.xyz";
    }
    
    
    
    
    @RequestMapping(value = "/users/index.xyz", method = RequestMethod.GET)
    public String getAdminTable(Model model, HttpServletRequest request) {
        if (!isRoot(request))
            return "redirect:/index.xyz";
        List<UserTable> users = userService.getList().stream().filter(u -> u.getUserType() == 2)
                .collect(Collectors.toList());
        model.addAttribute("userListNav", users);
        model.addAttribute("userList", users);
        return "root/RootPage";
    }

    @RequestMapping(value = {"/users/{hashCode}-index.xyz"}, method = RequestMethod.GET)
    public String editAdminTable(Model model, HttpServletRequest request,
                                @PathVariable(name = "hashCode") int hashCode) {
        if (!isRoot(request))
            return "redirect:/index.xyz";
        List<UserTable> users = userService.getList();
        for (UserTable user : users) {
            if (user.hashCode() == hashCode) {
                model.addAttribute("user", user);
                break;
            }
        }
        return "root/RootPage";
    }

    @RequestMapping(value = "/users/{userHashCode}/save", method = RequestMethod.GET)
    public String requestErrorUser() {
        return "redirect:/root/users/index.xyz";
    }

    @RequestMapping(value = "/users/{userHashCode}/save", method = RequestMethod.POST)
    public String updateAdminTable(Model model, HttpServletRequest request,
    							@RequestParam(name = "userPassword", defaultValue = "") String password,
                                  @RequestParam(name = "userIsActive", defaultValue = "1") String status,
                                  @RequestParam(name = "userEmail") String email,
                                  @RequestParam(name = "userPhone") String phone,
                                  @PathVariable(name = "userHashCode") int userHashCode) throws ParseException {
    	if (!isRoot(request))
            return "redirect:/index.xyz";
    	List<UserTable> users = userService.getList();
        UserTable oldUser = null;
        for (UserTable usertableEntity : users) {
            if (usertableEntity.hashCode() == userHashCode) {
                oldUser = usertableEntity;
                break;
            }
        }

        if (oldUser != null) {
        	
            if (!Common.INSTANCE.checkEmpty(email)  || !Common.INSTANCE.checkEmpty(phone)) {
                model.addAttribute("error", "You have to fill all the required fields. ");
                model.addAttribute("user",oldUser);
                return "root/RootPage";
            }
            oldUser.setIsActive(status.trim().equals("1")?(byte)1:(byte)0);
            oldUser.setUserEmail(email);
            oldUser.setUserPhone(phone);
            //---
            if (!Common.INSTANCE.checkValidEmail(email)) {
                model.addAttribute("error", "Your e-mail is not valid!");
                model.addAttribute("user",oldUser);
                return "root/RootPage";
            }
            
            if (!Common.INSTANCE.checkEmailForUpdate(users, oldUser)) {
                model.addAttribute("error", "This email has been used!");
                model.addAttribute("user",oldUser);
                return "root/RootPage";
            }
           
            if (!Common.INSTANCE.checkValidPhone(phone)) {
                model.addAttribute("error", "Your phone number is not valid!");
                model.addAttribute("user",oldUser);
                return "root/RootPage";
            }
           
            if (!Common.INSTANCE.checkPhoneForUpdate(users, oldUser)) {
                model.addAttribute("error", "This phone number has been used!");
                model.addAttribute("user",oldUser);
                return "root/RootPage";
            }

            
            userService.update(oldUser);
            model.addAttribute("success", "Moderator Update Successful!");
            model.addAttribute("user",oldUser);
        }
        return "root/RootPage";
    }
    
    @RequestMapping(value = {"users/{hashcode}-enable", "users/{hashcode}-disable"}, method = RequestMethod.GET)
    public String requestChangeAdminStatus(@PathVariable(name = "hashcode") int hashcode) {
        List<UserTable> users = userService.getList();
        for (UserTable user : users) {
            if (user.hashCode() == hashcode) {
                user.setIsActive(user.getIsActive()==1?(byte)0:(byte)1);
                userService.update(user);
                break;
            }
        }

        return "redirect:/root/users/index.xyz";
    }
    
    @RequestMapping(value = "users/adduser.xyz", method = RequestMethod.GET)
    public String rootAddAdmin( Model model, HttpServletRequest request) {
    	if (!isRoot(request))
            return "redirect:/index.xyz";
    	model.addAttribute("register", "register");
    	return "root/RootPage";
    }

    @RequestMapping(value = "users/adduser.xyz", method = RequestMethod.POST)
    public String addNewAdmin(Model model,  HttpServletRequest request, @RequestParam(name = "inputUserName") String username, 
                             @RequestParam(name = "inputPassword") String password,
                             @RequestParam(name = "inputConfirmPassword", defaultValue = "") String confirmPassword,
                             @RequestParam(name = "inputEmail") String email,
                             @RequestParam(name = "inputPhone") String phone,
                             final RedirectAttributes redirectAttributes) {
    	if (!isRoot(request))
            return "redirect:/index.xyz";
    	UserTable user = new UserTable();
        user.setUserPhone(phone);
        user.setUserEmail(email);
        user.setUserName(username);
        user.setUserPassword(password);
        user.setIsActive((byte)1);
        user.setUserType(2);
        model.addAttribute("nuser", user);
        //---Update 9/10/2017 12:18:03 AM
        //---
        List<UserTable> users = userService.getList();

        if (!Common.INSTANCE.checkEmpty(username)
                || !Common.INSTANCE.checkEmpty(password)
                || !Common.INSTANCE.checkEmpty(email)
                || !Common.INSTANCE.checkEmpty(phone)) {
            model.addAttribute("error", "You have to fill all the required fields!");
            model.addAttribute("register", "register");
            return "root/RootPage";
        }
        if (!Common.INSTANCE.checkMinLength(username) || !Common.INSTANCE.checkMinLength(password)) {
            model.addAttribute("error", "Your username and password must be at least 8 characters!");
            model.addAttribute("register", "register");
            return "root/RootPage";
        }
        
        if (!Common.INSTANCE.checkMaxLength(username) || !Common.INSTANCE.checkMaxLength(password)) {
            model.addAttribute("error", "Your username and password must be at most 16 characters!");
            model.addAttribute("register", "register");
            return "root/RootPage";
        }
        if (!Common.INSTANCE.checkValidUsername(username)) {
            model.addAttribute("error", "Your username must contain only letters and numbers!");
            model.addAttribute("register", "register");
            return "root/RootPage";
        }
        
        if (!Common.INSTANCE.checkExistUsername(users, user)) {
            model.addAttribute("error", "This username has already existed!");
            model.addAttribute("register", "register");
            return "root/RootPage";
        }
        //---Update 9/10/2017 12:18:03 AM


        if (!Common.INSTANCE.checkValidPassword(password)) {
            model.addAttribute("error", "Your password must contain at least a lower and an upper character!");
            model.addAttribute("register", "register");
            return "root/RootPage";
        }
        
        
        if (!confirmPassword.equals(password)) {
            model.addAttribute("confirmPassword", confirmPassword);
            model.addAttribute("error", "Your confirm password did not match!");
            model.addAttribute("register", "register");
            return "root/RootPage";
        }
        //---
        
        if (!Common.INSTANCE.checkValidEmail(email)) {
            model.addAttribute("error", "Your e-mail is not valid!");
            model.addAttribute("register", "register");
            return "root/RootPage";
        }
        

        if (!Common.INSTANCE.checkExistEmail(users, user)) {
            model.addAttribute("error", "This email has been used!");
            model.addAttribute("register", "register");
            return "root/RootPage";
        }
        
        if (!Common.INSTANCE.checkValidPhone(phone)) {
            model.addAttribute("error", "Your phone number is not valid!");
            model.addAttribute("register", "register");
            return "root/RootPage";
        }
        
        if (!Common.INSTANCE.checkExistPhone(users, user)) {
            model.addAttribute("error", "This phone number has been used!");
            model.addAttribute("register", "register");
            return "root/RootPage";
        }
        
        SubscriptionTable subscriptiontableEntity = new SubscriptionTable();
        try {
            password = Common.INSTANCE.getHashMD5(password);
        }
        catch(Exception e) {}
        user.setUserPassword(password);
        userService.save(user);

        users = userService.getList();

        subscriptiontableEntity
                .setUserTable(users.stream().filter(u -> u.getUserName().equals(username)).findFirst().orElseThrow(null));

        subService.save(subscriptiontableEntity);
        model.addAttribute("success","Moderator Added Successful!");
        redirectAttributes.addFlashAttribute("nuser", user);
        return "redirect:/root/users/index.xyz";

    }
    
    // ## ==================== APPLICATION VERSION MANAGEMENT ==================== ##

    @RequestMapping(value = { "/apps"}, method = RequestMethod.GET)
    public String redirectAppRequest() {
    	return "redirect:/root/apps/index.xyz";
    }
    
    
    
    
    @RequestMapping(value = "/apps/index.xyz", method = RequestMethod.GET)
    public String getApplicationVersionTable(Model model, HttpServletRequest request) {
        if (!isRoot(request))
            return "redirect:/index.xyz";
        List<Application_Version> apps = appService.getList().stream()
                .collect(Collectors.toList());
        model.addAttribute("appListNav", apps);
        model.addAttribute("appList", apps);
        return "root/RootPage";
    }

    @RequestMapping(value = {"/apps/{appVersionID}-index.xyz"}, method = RequestMethod.GET)
    public String editApplicationVersion(Model model, HttpServletRequest request,
                                @PathVariable(name = "appVersionID") int id) {
        if (!isRoot(request))
            return "redirect:/index.xyz";
        List<Application_Version> apps = appService.getList();
        for (Application_Version app : apps) {
            if (app.getAppVersionID() == id) {
                model.addAttribute("app", app);
                break;
            }
        }
        return "root/RootPage";
    }

    @RequestMapping(value = "/apps/{appVersionID}/save", method = RequestMethod.GET)
    public String requestErrorApp() {
        return "redirect:/root/apps/index.xyz";
    }

    @RequestMapping(value = "/apps/{appVersionID}/save", method = RequestMethod.POST)
    public String updateApplicationVersion(Model model, HttpServletRequest request,
    							@RequestParam(name = "appVersionNumber") String number,
                                  @RequestParam(name = "changeLog") String log,
                                  @RequestParam(name = "appLink") String link,
                                  @PathVariable(name = "appVersionID") String id) throws ParseException {
        List<Application_Version> apps = appService.getList();
        Application_Version oldApp = null;
        int appId = Integer.parseInt(id);
        for (Application_Version appVersionEntity : apps) {
            if (appVersionEntity.getAppVersionID() == appId) {
                oldApp = appVersionEntity;
                break;
            }
        }

        
        if (oldApp != null) {
        	
            
            if (!Common.INSTANCE.checkEmpty(link) || !Common.INSTANCE.checkEmpty(log)) {
            	model.addAttribute("error", "You have to fill all the required fields!");
            	model.addAttribute("app", oldApp);
            	return "root/RootPage";
            }

            oldApp.setAppVersionNumber(number);
            oldApp.setChangeLog(log);
            oldApp.setAppLink(link);
            appService.update(oldApp);
            model.addAttribute("success", "Application Version Update Successful!");
            model.addAttribute("app", oldApp);
            return "root/RootPage";
        }
        return "redirect:/root/apps";
    }
    
    
    @RequestMapping(value = "apps/addApp.xyz", method = RequestMethod.GET)
    public String addApp( Model model, HttpServletRequest request) {
    	model.addAttribute("addApp", "addApp");
    	return "root/RootPage";
    }

    @RequestMapping(value = "apps/addApp.xyz", method = RequestMethod.POST)
    public String addNewApplicationVersion(Model model, @RequestParam(name = "inputAppVersionNumber") String number,
                             @RequestParam(name = "inputChangeLog") String log,
                            @RequestParam(name = "inputAppLink") String link,
                             final RedirectAttributes redirectAttributes) {
    	Application_Version app = new Application_Version();
        app.setAppVersionNumber(number);
        app.setChangeLog(log);
        app.setAppLink(link);
        model.addAttribute("napp", app);
        //---Update 9/10/2017 12:18:03 AM
        //---
        List<Application_Version> apps = appService.getList();

        if (number.isEmpty() || number == null || log.isEmpty() || log == null || link.isEmpty() || link == null) {
            model.addAttribute("error", "You have to fill all the required fields!");
            model.addAttribute("addApp", "addApp");
            return "root/RootPage";
        }
       
        appService.save(app);


        redirectAttributes.addFlashAttribute("napp", app);
        return "redirect:/root/apps/index.xyz";

    }
}
