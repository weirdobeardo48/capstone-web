package capstone.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class ErrorController {

    @GetMapping(value = "/errors")
    public String errorGetMapping() {
        return "redirect:/error.xyz";
    }

    @PostMapping(value = "/errors")
    public String errorPostMapping() {
        return "redirect:/error.xyz";
    }

    @GetMapping(value = "/error.xyz")
    public String error() {
        return "errorPage";
    }
}
