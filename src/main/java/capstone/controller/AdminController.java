package capstone.controller;

import capstone.common.Common;
import capstone.common.Common_const;
import capstone.model.SubscriptionTable;
import capstone.model.TransactionTable;
import capstone.model.PackageTable;
import capstone.model.UserTable;
import capstone.service.EntityService;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import java.security.NoSuchAlgorithmException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Controller
@RequestMapping("/admin")
public class AdminController {
	
    @Resource(name = "usertableService")
    private EntityService<UserTable> userService;
    @Resource(name = "transactiontableService")
    private EntityService<TransactionTable> transactionService;
    @Resource(name = "subscriptiontableService")
    private EntityService<SubscriptionTable> subService;
    @Resource(name = "packagetableService")
    private EntityService<PackageTable> packageService;

    @Resource(name = "transactiontableService")
    private EntityService<TransactionTable> transService;

    private boolean isAdmin(HttpServletRequest request) {
        if (request.getSession().getAttribute("currentUser") == null)
            return false;
        UserTable user = (UserTable) request.getSession().getAttribute("currentUser");
        return Common.INSTANCE.isAdmin(user);
    }
    
 // ## ==================== PROFILE MANAGEMENT ==================== ##

    // display user profile
    @RequestMapping(value = "/profiles", method = RequestMethod.GET)
    public String redirectAdminProfile() {
        return "redirect:/admin/profiles/index.xyz";
    }

    @RequestMapping(value = {"/profiles/index.xyz"}, method = RequestMethod.GET)
    public String editAdminProfile(Model model, HttpServletRequest request) {
        if (!isAdmin(request))
            return "redirect:/index.xyz";
        UserTable user = (UserTable) request.getSession().getAttribute("currentUser");
        model.addAttribute("puser", user);
        model.addAttribute("userProfile", "userProfile");
        return "admin/AdminPage";
    }
 
    
    @RequestMapping(value = {"/profile/adminProfilePassword.xyz"}, method = RequestMethod.GET)
    public String editAdminPassword(Model model, HttpServletRequest request) {
        if (!isAdmin(request))
            return "redirect:/index.xyz";
        UserTable user = (UserTable) request.getSession().getAttribute("currentUser");
        model.addAttribute("puser", user);
        model.addAttribute("userProfilePassword", "userProfilePassword");
        return "admin/AdminPage";
    }

    // save change of user profile
    @RequestMapping(value = {"/profile/saveProfile.xyz"}, method = RequestMethod.POST)
    public String updateAdminProfile(@ModelAttribute UserTable puser, Model model, HttpServletRequest request) {
        if (!isAdmin(request))
            return "redirect:/index.xyz";

        UserTable currentUser = (UserTable) request.getSession().getAttribute("currentUser");

        List<UserTable> users = userService.getList();
        UserTable oldUser = null;
        for (UserTable usertableEntity : users) {
            if (usertableEntity.getUserName().equals(currentUser.getUserName())) {
                oldUser = usertableEntity;
                break;
            }
        }

        if (oldUser != null) {
        	puser.setUserID(oldUser.getUserID());
            if (!Common.INSTANCE.checkEmpty(puser.getUserEmail()) || !Common.INSTANCE.checkEmpty(puser.getUserPhone())) {
            	model.addAttribute("error", "You have to fill all the required fields!");
                model.addAttribute("puser", puser);
                model.addAttribute("userProfile", "userProfile");
                return "admin/AdminPage";
            }
            oldUser.setUserEmail(puser.getUserEmail());
            oldUser.setUserPhone(puser.getUserPhone());
            if (!Common.INSTANCE.checkValidEmail(puser.getUserEmail())) {
                // currentUser.setUserEmail(oldUser.getUserEmail());
                model.addAttribute("error", "Your e-mail is not valid!");
                model.addAttribute("puser", puser);
                model.addAttribute("userProfile", "userProfile");
                return "admin/AdminPage";
            }
            
            if (!Common.INSTANCE.checkEmailForUpdate(users,puser)) {
                // currentUser.setUserAddress(oldUser.getUserAddress());
                model.addAttribute("error", "This email has been used!");
                model.addAttribute("puser", puser);
                model.addAttribute("userProfile", "userProfile");
                return "admin/AdminPage";
            }
            
            if (!Common.INSTANCE.checkValidPhone(puser.getUserPhone())) {
                // currentUser.setUserPhoneNo(oldUser.getUserPhoneNo());
                model.addAttribute("error", "Your phone number is not valid!");
                model.addAttribute("puser", puser);
                model.addAttribute("userProfile", "userProfile");
                return "admin/AdminPage";
            }
            
            if (!Common.INSTANCE.checkPhoneForUpdate(users, puser)) {
                // currentUser.setUserAddress(oldUser.getUserAddress());
                model.addAttribute("error", "This phone number has been used!");
                model.addAttribute("puser", puser);
                model.addAttribute("userProfile", "userProfile");
                return "admin/AdminPage";
            }
            currentUser.setUserEmail(puser.getUserEmail());
            currentUser.setUserPhone(puser.getUserPhone());
            userService.update(oldUser);
            model.addAttribute("puser", puser);
            model.addAttribute("userProfile", "userProfile");
            model.addAttribute("success", "Your profile is updated successfully!");
            return "admin/AdminPage";
        }
        model.addAttribute("puser", puser);
        model.addAttribute("adminProfile", "adminProfile");
        return "admin/AdminPage";
    }

    // save change of user password
    @RequestMapping(value = {"/profile/savePassword.xyz"}, method = RequestMethod.POST)
    public String updateAdminPassword(Model model, HttpServletRequest request,
                                     @RequestParam(required = false, name = "userOldPassword") String oldPassword,
                                     @RequestParam(required = false, name = "userNewPassword") String newPassword,
                                     @RequestParam(required = false, name = "userConfirmNewPassword") String confirmNewPassword) throws NoSuchAlgorithmException {
        if (!isAdmin(request))
            return "redirect:/index.xyz";
        if (oldPassword == null || newPassword == null || confirmNewPassword == null) {
            model.addAttribute("userProfilePassword", "userProfilePassword");
            model.addAttribute("error", "You have to fill all the required fields!");
            return "admin/AdminPage";
        }
       

        UserTable currentUser = (UserTable) request.getSession().getAttribute("currentUser");

        List<UserTable> users = userService.getList();
        UserTable oldUser = null;
        for (UserTable usertableEntity : users) {
            if (usertableEntity.getUserName().equals(currentUser.getUserName())) {
                oldUser = usertableEntity;
                break;
            }
        }

        if (oldUser != null) {
            
            
            if (oldPassword.isEmpty() || newPassword.isEmpty() || confirmNewPassword.isEmpty()) {
                model.addAttribute("error", "You have to fill all the required fields!");
                model.addAttribute("puser", oldUser);
                model.addAttribute("userProfilePassword", "userProfilePassword");
                return "admin/AdminPage";
            }
            oldUser.setUserPassword(newPassword);
            if (!Common.INSTANCE.getHashMD5(oldPassword).equals(oldUser.getUserPassword())) {
                model.addAttribute("error", "Your old password is wrong!");
                model.addAttribute("puser", oldUser);
                model.addAttribute("userProfilePassword", "userProfilePassword");
                return "admin/AdminPage";
            }
            if (oldPassword.equals(newPassword)) {
                model.addAttribute("error", "New password must be different from old password!");
                model.addAttribute("puser", oldUser);
                model.addAttribute("userProfilePassword", "userProfilePassword");
                return "admin/AdminPage";
            }

            if (!Common.INSTANCE.checkMinLength(newPassword)) {
                model.addAttribute("error", "Your password must be at least 8 characters!");
                model.addAttribute("puser", oldUser);
                model.addAttribute("userProfilePassword", "userProfilePassword");
                return "admin/AdminPage";
            }
            if (!Common.INSTANCE.checkMaxLength(newPassword)) {
                model.addAttribute("error", "Your password must be at most 16 characters!");
                model.addAttribute("puser", oldUser);
                model.addAttribute("userProfilePassword", "userProfilePassword");
                return "admin/AdminPage";
            }
            
            if (!Common.INSTANCE.checkValidPassword(newPassword)) {
                model.addAttribute("error", "Your password must contain at least a lower and an upper character!");
                model.addAttribute("puser", oldUser);
                model.addAttribute("userProfilePassword", "userProfilePassword");
                return "admin/AdminPage";
            }

            if (!newPassword.equals(confirmNewPassword)) {
                model.addAttribute("error", "Your confirm password did not match!");
                model.addAttribute("puser", oldUser);
                model.addAttribute("userProfilePassword", "userProfilePassword");
                return "admin/AdminPage";
            }


            oldUser.setUserPassword(Common.INSTANCE.getHashMD5(currentUser.getUserPassword()));
            currentUser.setUserPassword(Common.INSTANCE.getHashMD5(currentUser.getUserPassword()));
            userService.update(oldUser);
            model.addAttribute("puser", oldUser);
            model.addAttribute("userProfilePassword", "userProfilePassword");
            model.addAttribute("updateUserPasswordSuccess", "Your password is updated successfully!");
            return "admin/AdminPage";
        }

        model.addAttribute("puser", currentUser);
        model.addAttribute("userProfilePassword", "userProfilePassword");
        return "admin/AdminPage";
        
    }

    // ## ==================== PROFILE MANAGEMENT ==================== ##
    
    // ## ==================== USER MANAGEMENT ==================== ##

    @RequestMapping(value = {"", "/", ".xyz", "/index", "/index.xyz", "/users"}, method = RequestMethod.GET)
    public String redirectUserRequest() {
    	return "redirect:/admin/users/index.xyz";
    }
    
    
    
    
    @RequestMapping(value = "/users/index.xyz", method = RequestMethod.GET)
    public String getUserTable(Model model, HttpServletRequest request) {
        if (!isAdmin(request))
            return "redirect:/index.xyz";
        List<UserTable> users = userService.getList().stream().filter(u -> u.getUserType() == 1)
                .collect(Collectors.toList());
        model.addAttribute("userListNav", users);
        model.addAttribute("userList", users);
        return "admin/AdminPage";
    }
    
    
   
    @RequestMapping(value = {"/users/{hashCode}-index.xyz"}, method = RequestMethod.GET)
    public String editUserTable(Model model, HttpServletRequest request,
                                @PathVariable(name = "hashCode") int hashCode) {
        if (!isAdmin(request))
            return "redirect:/index.xyz";
        List<UserTable> users = userService.getList();
        for (UserTable user : users) {
            if (user.hashCode() == hashCode) {
                model.addAttribute("user", user);
                break;
            }
        }
        return "admin/AdminPage";
    }

    @RequestMapping(value = "/users/{userHashCode}/save", method = RequestMethod.GET)
    public String requestErrorUser() {
        return "redirect:/admin/users/index.xyz";
    }

    @RequestMapping(value = "/users/{userHashCode}/save", method = RequestMethod.POST)
    public String updateUserTable(Model model, HttpServletRequest request,
    							@RequestParam(name = "userPassword", defaultValue = "") String password,
                                  @RequestParam(name = "userIsActive", defaultValue = "1") String status,
                                  @RequestParam(name = "userEmail", defaultValue = "") String email,
                                  @RequestParam(name = "userPhone", defaultValue = "") String phone,
                                  @RequestParam(name = "userExpiredDate", defaultValue = "") String date,
                                  @PathVariable(name = "userHashCode") int userHashCode) throws ParseException {
        List<UserTable> users = userService.getList();
        UserTable oldUser = null;
        for (UserTable usertableEntity : users) {
            if (usertableEntity.hashCode() == userHashCode) {
                oldUser = usertableEntity;
                break;
            }
        }

        
        if (oldUser != null) {
           
            if (!Common.INSTANCE.checkEmpty(email) || !Common.INSTANCE.checkEmpty(phone)) {
                model.addAttribute("error", "You have to fill all the required fields!");
                model.addAttribute("user",oldUser);
                return "admin/AdminPage";
            }
            oldUser.setIsActive(status.trim().equals("1")?(byte)1:(byte)0);
            oldUser.setUserEmail(email);
            oldUser.setUserPhone(phone);
            if (!Common.INSTANCE.checkEmailForUpdate(users, oldUser)) {
                model.addAttribute("error", "This email has been used!");
                model.addAttribute("user",oldUser);
                return "admin/AdminPage";
            }
            
            //---
            if (!Common.INSTANCE.checkValidEmail(email)) {
                model.addAttribute("error", "Your e-mail is not valid!");
                model.addAttribute("user",oldUser);
                return "admin/AdminPage";
            }
            
            if (!Common.INSTANCE.checkPhoneForUpdate(users, oldUser)) {
                model.addAttribute("error", "This phone number has been used!");
                model.addAttribute("user",oldUser);
                return "admin/AdminPage";
            }
           
            if (!Common.INSTANCE.checkValidPhone(phone)) {
                model.addAttribute("error", "Your phone number is not valid!");
                model.addAttribute("user",oldUser);
                return "admin/AdminPage";
            }
           
            if(!date.isEmpty()){
                SubscriptionTable sub = oldUser.getSubscriptionTable();
                Date experyDate = new SimpleDateFormat("yyyy-MM-dd").parse(date);
                sub.setExpiredDate(new java.sql.Date(experyDate.getTime()));
                subService.update(sub);
            }
            
            else {
            	model.addAttribute("error", "Your expired date is empty");
            	model.addAttribute("user",oldUser);
                return "admin/AdminPage";
            }
            userService.update(oldUser);
            model.addAttribute("success", "User Update Successful");
        	model.addAttribute("user",oldUser);
        }
        return "admin/AdminPage";
    }
    
    
    //View user transaction
    @RequestMapping(value = {"/users/{hashCode}tran-index.xyz"}, method = RequestMethod.GET)
    public String viewTransactionTable(Model model, HttpServletRequest request,
                                @PathVariable(name = "hashCode") int hashCode) {
        if (!isAdmin(request))
            return "redirect:/index.xyz";
        List<UserTable> users = userService.getList();
        for (UserTable user : users) {
            if (user.hashCode() == hashCode) {
            	List<TransactionTable> trans = transactionService.getList().stream().filter(u -> u.getUserTable().getUserID() == user.getUserID())
                        .collect(Collectors.toList());
            	model.addAttribute("hash",hashCode);
                model.addAttribute("listTransaction", trans);
                break;
            }
        }
        return "admin/AdminPage";
    }

    @RequestMapping(value = {"users/{hashcode}-enable", "users/{hashcode}-disable"}, method = RequestMethod.GET)
    public String requestChangeUserStatus(@PathVariable(name = "hashcode") int hashcode) {
        List<UserTable> users = userService.getList();
        for (UserTable user : users) {
            if (user.hashCode() == hashcode) {
                user.setIsActive(user.getIsActive()==1?(byte)0:(byte)1);
                userService.update(user);
                break;
            }
        }

        return "redirect:/admin/users/index.xyz";
    }

    
    // change user plan page
    @RequestMapping(value = {"users/{hashcode}-plan.xyz"}, method = RequestMethod.GET)
    public String editUserPlan(Model model, @PathVariable(name = "hashcode") int hashcode, HttpServletRequest request) {
        if (!isAdmin(request))
            return "redirect:/index.xyz";
        List<UserTable> users = userService.getList();
        for (UserTable user : users) {
            if (user.hashCode() == hashcode) {
                model.addAttribute("user", user);
                break;
            }
        }
        // model.addAttribute("user", user);
        model.addAttribute("userPlan", "userPlan");
        addListPackages(model);
        return "admin/AdminPage";
    }

    @RequestMapping(value = {"users/savePlan.xyz"}, method = RequestMethod.POST)
    public String updateUserPlan(Model model, HttpServletRequest request,
                                 @RequestParam(name = "packages", defaultValue = "", required = true) int packageID,
                                 @RequestParam(name = "userID", required = true) int userID) {
        if (!isAdmin(request))
            return "redirect:/index.xyz";

        if (packageID == -1) {
            return "redirect:/admin/users";
        }

        Common.INSTANCE.updateSubscription(userService, packageService, subService, 
                transService, userID, packageID,
                userService.findById(userID).getSubscriptionTable().getSubscriptionID(),
                Common_const.BANK_TRANSFER);

        return "redirect:/admin/users";
    }
    
    
    private void addListPackages(Model model) {
        List<PackageTable> packs = packageService.getList();
        model.addAttribute("packs", packs);
    }
    
    
    @RequestMapping(value = "users/adduser.xyz", method = RequestMethod.GET)
    public String adminAddUser( Model model, HttpServletRequest request) {
    	if (!isAdmin(request))
            return "redirect:/index.xyz";
    	model.addAttribute("register", "register");
        return "admin/AdminPage";
    }

    @RequestMapping(value = "users/adduser.xyz", method = RequestMethod.POST)
    public String addNewUser(Model model, HttpServletRequest request, @RequestParam(name = "inputUserName") String username,
                             @RequestParam(name = "inputPassword") String password,
                             @RequestParam(name = "inputConfirmPassword", defaultValue = "") String confirmPassword,
                             @RequestParam(name = "inputEmail") String email,
                             @RequestParam(name = "inputPhone") String phone,
                             final RedirectAttributes redirectAttributes) {
    	 if (!isAdmin(request))
             return "redirect:/index.xyz";
    	UserTable user = new UserTable();
        user.setUserPhone(phone);
        user.setUserEmail(email);
        user.setUserName(username);
        user.setUserPassword(password);
        user.setIsActive((byte)1);
        user.setUserType(1);
        model.addAttribute("nuser", user);
        //---Update 9/10/2017 12:18:03 AM
        //---
        List<UserTable> users = userService.getList();

        if (!Common.INSTANCE.checkEmpty(username) || !Common.INSTANCE.checkEmpty(password)
        		|| !Common.INSTANCE.checkEmpty(email) || !Common.INSTANCE.checkEmpty(phone)){
            model.addAttribute("error", "You have to fill all the required fields!");
            model.addAttribute("register", "register");
            return "admin/AdminPage";
        }
        if (!Common.INSTANCE.checkMinLength(username) || !Common.INSTANCE.checkMinLength(password)) {
            model.addAttribute("error", "Your username and password must be at least 8 characters!");
            model.addAttribute("register", "register");
            return "admin/AdminPage";
        }
        //---Update 9/10/2017 12:18:03 AM
        
        if (!Common.INSTANCE.checkMaxLength(username) || !Common.INSTANCE.checkMaxLength(password)) {
            model.addAttribute("error", "Your username and password must be at most 16 characters!");
            model.addAttribute("register", "register");
            return "admin/AdminPage";
        }
        
        if (!Common.INSTANCE.checkValidUsername(username)) {
            model.addAttribute("error", "Your username must contain only letters and numbers!");
            model.addAttribute("register", "register");
            return "admin/AdminPage";
        }
        
        if (!Common.INSTANCE.checkExistUsername(users, user)) {
            model.addAttribute("error", "Usename has been existed!");
            model.addAttribute("register", "register");
            return "admin/AdminPage";
        }
        
        if (!Common.INSTANCE.checkValidPassword(password)) {
            model.addAttribute("error", "Your password must contain at least a lower and an upper character!");
            model.addAttribute("register", "register");
            return "admin/AdminPage";
        }
        
        if (!confirmPassword.equals(password)) {
            model.addAttribute("confirmPassword", confirmPassword);
            model.addAttribute("error", "Password does not match the confirm password!");
            model.addAttribute("register", "register");
            return "admin/AdminPage";
        }
        //---
        if (!Common.INSTANCE.checkValidEmail(email)) {
            model.addAttribute("error", "Your e-mail is not valid!");
            model.addAttribute("register", "register");
            return "admin/AdminPage";
        }
        
        if (!Common.INSTANCE.checkExistEmail(users, user)) {
            model.addAttribute("error", "Email has been used!");
            model.addAttribute("register", "register");
            return "admin/AdminPage";
        }
        
        if (!Common.INSTANCE.checkValidPhone(phone)) {
            model.addAttribute("error", "Your phone number is not valid!");
            model.addAttribute("register", "register");
            return "admin/AdminPage";
        }

        
        if (!Common.INSTANCE.checkExistPhone(users, user)) {
            model.addAttribute("error", "This phone number has been used!");
            model.addAttribute("register", "register");
            return "admin/AdminPage";
        }
        
        SubscriptionTable subscriptiontableEntity = new SubscriptionTable();
       
        try {
            password = Common.INSTANCE.getHashMD5(password);
        }
        catch(Exception e) {};
        user.setUserPassword(password);
        userService.save(user);

        users = userService.getList();

        subscriptiontableEntity
                .setUserTable(users.stream().filter(u -> u.getUserName().equals(username)).findFirst().orElseThrow(null));

        subService.save(subscriptiontableEntity);	
        redirectAttributes.addFlashAttribute("nuser", user);
        model.addAttribute("success","User added successful!");
        return "redirect:/admin/users/index.xyz";

    }
    
    
    // ## ==================== END USER MANAGEMENT ==================== ##
    
 // ## ==================== PACKAGE MANAGEMENT ==================== ##

    @RequestMapping(value = {"/packs"}, method = RequestMethod.GET)
    public String redirectPackageRequest() {
    	return "redirect:/admin/packs/index.xyz";
    }
    
    @RequestMapping(value = "/packs/index.xyz", method = RequestMethod.GET)
    public String getPackageTable(Model model, HttpServletRequest request) {
        if (!isAdmin(request))
            return "redirect:/index.xyz";
        List<PackageTable> packs = packageService.getList().stream()
                .collect(Collectors.toList());
        model.addAttribute("packListNav", packs);
        model.addAttribute("packList", packs);
        return "admin/AdminPage";
    }

    @RequestMapping(value = {"/packs/{hashCode}-index.xyz"}, method = RequestMethod.GET)
    public String editPackageTable(Model model, HttpServletRequest request,
                                @PathVariable(name = "hashCode") int hashCode) {
        if (!isAdmin(request))
            return "redirect:/index.xyz";
        List<PackageTable> packs = packageService.getList();
        for (PackageTable pack : packs) {
            if (pack.hashCode() == hashCode) {
                model.addAttribute("pack", pack);
                break;
            }
        }
        return "admin/AdminPage";
    }

    @RequestMapping(value = "/packs/{packageHashCode}/save", method = RequestMethod.GET)
    public String requestErrorPackage() {
        return "redirect:/admin/packs/index.xyz";
    }

    @RequestMapping(value = "/packs/{packageHashCode}/save", method = RequestMethod.POST)
    public String updatePackageTable(Model model, @RequestParam(name = "packageName", defaultValue = "") String name,
                                  @RequestParam(name = "packagePrice", defaultValue = "0") String price,
                                  @RequestParam(name = "daysOfUsing", defaultValue = "0") String days,
                                  @RequestParam(name = "packageDescription", defaultValue = "") String des,
                                  @PathVariable(name = "packageHashCode") int userHashCode) throws ParseException {
        
    	List<PackageTable> packs = packageService.getList();
        PackageTable oldPackage = null;
        for (PackageTable packageTable : packs) {
            if (packageTable.hashCode() == userHashCode) {
                oldPackage = packageTable;
                break;
            }
        }

        if (oldPackage != null) {
        	if (Integer.parseInt(price) < 1) {
        		 model.addAttribute("error", "Package price must be greater than 0!");
                 model.addAttribute("pack",oldPackage);
                 return "admin/AdminPage";
        	}
        	if (Integer.parseInt(days) < 1) {
        		model.addAttribute("error", "Days of using must be greater than 0!");
        		model.addAttribute("pack",oldPackage);
        		return "admin/AdminPage";
        	}
        	oldPackage.setPackageName(name);
        	oldPackage.setPackagePrice(Integer.parseInt(price));
        	oldPackage.setDaysOfUsing(Integer.parseInt(days));
        	oldPackage.setPackageDescription(des);
        	packageService.update(oldPackage);
        	model.addAttribute("pack",oldPackage);
        	model.addAttribute("success", "Package updated successful!");
        }
        
        return "admin/AdminPage";
    }
    

    // ## ==================== END PACKAGE MANAGEMENT ==================== ##
    
 // ## ==================== TRANSACTION MANAGEMENT ==================== ##

    @RequestMapping(value = {"/transactions"}, method = RequestMethod.GET)
    public String redirectTransactionRequest() {
    	return "redirect:/admin/transactions/index.xyz";
    }
    
    @RequestMapping(value = "/transactions/index.xyz", method = RequestMethod.GET)
    public String getTransactionTable(Model model, HttpServletRequest request) {
        if (!isAdmin(request))
            return "redirect:/index.xyz";
        List<TransactionTable> list = transactionService.getList().stream()
                .collect(Collectors.toList());
        model.addAttribute("listTransactions", list);
        return "admin/AdminPage";
    }

    // ## ==================== END TRANSACTION MANAGEMENT ==================== ##
}
