package capstone.controller;

import java.util.*;
import javax.mail.*;
import javax.mail.internet.*;

import capstone.common.*;
import capstone.model.TransactionTable;
import capstone.model.UserTable;
import capstone.service.EntityService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import java.security.NoSuchAlgorithmException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.stream.Collectors;

@Controller
@RequestMapping("/user")
public class UserController {
    @Resource(name = "usertableService")
    private EntityService<UserTable> userService;
    @Resource(name = "transactiontableService")
    private EntityService<TransactionTable> transactionService;

    private boolean isAdmin(HttpServletRequest request) {
        if (request.getSession().getAttribute("currentUser") == null)
            return false;
        UserTable user = (UserTable) request.getSession().getAttribute("currentUser");
        return !Common.INSTANCE.isAdmin(user);
    }

    
    // ---------------- USER PROFILE -------------------
    @RequestMapping(value = {"", "/", "/users"}, method = RequestMethod.GET)
    public String redirectShopRequest() {
        return "redirect:/user/profile/userProfile.xyz";
    }
    
    // display user profile
    @RequestMapping(value = "/profile", method = RequestMethod.GET)
    public String redirectUserProfile() {
        return "redirect:/user/profile/userProfile.xyz";
    }

    @RequestMapping(value = {"/profile/userProfile.xyz"}, method = RequestMethod.GET)
    public String editUserProfile(Model model, HttpServletRequest request) {
        if (!isAdmin(request))
            return "redirect:/index.xyz";
        UserTable user = (UserTable) request.getSession().getAttribute("currentUser");
        model.addAttribute("user", user);
        model.addAttribute("userProfile", "userProfile");
        return "user/userPage";
    }

    // change user password page

    @RequestMapping(value = {"/profile/userProfilePassword.xyz"}, method = RequestMethod.GET)
    public String editUserPassword(Model model, HttpServletRequest request) {
        if (!isAdmin(request))
            return "redirect:/index.xyz";
        UserTable user = (UserTable) request.getSession().getAttribute("currentUser");
        model.addAttribute("user", user);
        model.addAttribute("userProfilePassword", "userProfilePassword");
        return "user/userPage";
    }

    // save change of user profile
    @RequestMapping(value = {"/profile/saveProfile.xyz"}, method = RequestMethod.POST)
    public String updateUserProfile(@ModelAttribute UserTable user, Model model, HttpServletRequest request) {
        if (!isAdmin(request))
            return "redirect:/index.xyz";
        UserTable currentUser = (UserTable) request.getSession().getAttribute("currentUser");

        List<UserTable> users = userService.getList();
        UserTable oldUser = null;
        for (UserTable usertableEntity : users) {
            if (usertableEntity.getUserName().equals(currentUser.getUserName())) {
                oldUser = usertableEntity;
                break;
            }
        }

        if (oldUser != null) {
            user.setUserID(currentUser.getUserID());
            if (!Common.INSTANCE.checkEmpty(user.getUserEmail())
                    || !Common.INSTANCE.checkEmpty(user.getUserPhone())) {
                model.addAttribute("error", "You have to fill all the required fields!");
                model.addAttribute("user", user);
                model.addAttribute("userProfile", "userProfile");
                return "user/userPage";
            }
            oldUser.setUserEmail(user.getUserEmail());
            oldUser.setUserPhone(user.getUserPhone());
            if (!Common.INSTANCE.checkValidEmail(user.getUserEmail())) {
                // currentUser.setUserEmail(oldUser.getUserEmail());
                model.addAttribute("error", "Your email is not valid!");
                model.addAttribute("user", user);
                model.addAttribute("userProfile", "userProfile");
                return "user/userPage";
            }
            if (!Common.INSTANCE.checkValidPhone(user.getUserPhone())) {
                // currentUser.setUserPhoneNo(oldUser.getUserPhoneNo());
                model.addAttribute("error", "Your phone number is not valid!");
                model.addAttribute("user", user);
                model.addAttribute("userProfile", "userProfile");
                return "user/userPage";
            }
            if (!Common.INSTANCE.checkPhoneForUpdate(users, currentUser)) {
                // currentUser.setUserAddress(oldUser.getUserAddress());
                model.addAttribute("error", "This phone number has been used!");
                model.addAttribute("user", user);
                model.addAttribute("userProfile", "userProfile");
                return "user/userPage";
            }
            if (!Common.INSTANCE.checkEmailForUpdate(users, currentUser)) {
                // currentUser.setUserAddress(oldUser.getUserAddress());
                model.addAttribute("error", "This email has been used!");
                model.addAttribute("user", user);
                model.addAttribute("userProfile", "userProfile");
                return "user/userPage";
            }
            currentUser.setUserEmail(user.getUserEmail());
            currentUser.setUserPhone(user.getUserPhone());
            userService.update(oldUser);
            model.addAttribute("user", oldUser);
            model.addAttribute("userProfile", "userProfile");
            model.addAttribute("success", "Your profile is updated successfully!");
            return "user/userPage";
        }
        model.addAttribute("user", user);
        model.addAttribute("userProfile", "userProfile");
        return "user/userPage";
    }

    // save change of user password
    @RequestMapping(value = {"/profile/savePassword.xyz"}, method = RequestMethod.POST)
    public String updateUserPassword(Model model, HttpServletRequest request,
                                     @RequestParam(required = false, name = "userOldPassword") String oldPassword,
                                     @RequestParam(required = false, name = "userNewPassword") String newPassword,
                                     @RequestParam(required = false, name = "userConfirmNewPassword") String confirmNewPassword) throws NoSuchAlgorithmException {
        if (!isAdmin(request))
            return "redirect:/index.xyz";
        if (oldPassword == null || newPassword == null || confirmNewPassword == null) {
            model.addAttribute("userProfilePassword", "userProfilePassword");
            model.addAttribute("error", "You have to fill all the required fields!");
            return "user/userPage";
        }
       

        UserTable currentUser = (UserTable) request.getSession().getAttribute("currentUser");

        List<UserTable> users = userService.getList();
        UserTable oldUser = null;
        for (UserTable usertableEntity : users) {
            if (usertableEntity.getUserName().equals(currentUser.getUserName())) {
                oldUser = usertableEntity;
                break;
            }
        }

        if (oldUser != null) {
            if (oldPassword.isEmpty() || newPassword.isEmpty() || confirmNewPassword.isEmpty()) {
                model.addAttribute("error", "You have to fill all the required fields!");
                model.addAttribute("user", oldUser);
                model.addAttribute("userProfilePassword", "userProfilePassword");
                return "user/userPage";
            }
            currentUser.setUserPassword(newPassword);
            if (!Common.INSTANCE.getHashMD5(oldPassword).equals(oldUser.getUserPassword())) {
                model.addAttribute("error", "Your password is wrong!");
                model.addAttribute("user", oldUser);
                model.addAttribute("userProfilePassword", "userProfilePassword");
                return "user/userPage";
            }
            if (oldPassword.equals(newPassword)) {
                model.addAttribute("error", "New password must be different from old password!");
                model.addAttribute("user", oldUser);
                model.addAttribute("userProfilePassword", "userProfilePassword");
                return "user/userPage";
            }

            if (!Common.INSTANCE.checkMinLength(newPassword)) {
                model.addAttribute("error", "Your password must be at least 8 characters!");
                model.addAttribute("user", oldUser);
                model.addAttribute("userProfilePassword", "userProfilePassword");
                return "user/userPage";
            }
            
            if (!Common.INSTANCE.checkMaxLength(newPassword)) {
                model.addAttribute("error", "Your password must be at most 16 characters!");
                model.addAttribute("user", oldUser);
                model.addAttribute("userProfilePassword", "userProfilePassword");
                return "user/userPage";
            }
            
            if (!Common.INSTANCE.checkValidPassword(newPassword)) {
                model.addAttribute("error", "Your password must contain at least a lower and an upper character!");
                model.addAttribute("user", oldUser);
                model.addAttribute("userProfilePassword", "userProfilePassword");
                return "user/userPage";
            }

            if (!newPassword.equals(confirmNewPassword)) {
                model.addAttribute("error", "Your confirm password did not match!");
                model.addAttribute("user", oldUser);
                model.addAttribute("userProfilePassword", "userProfilePassword");
                return "user/userPage";
            }


            oldUser.setUserPassword(Common.INSTANCE.getHashMD5(currentUser.getUserPassword()));
            currentUser.setUserPassword(Common.INSTANCE.getHashMD5(currentUser.getUserPassword()));
            userService.update(oldUser);

            model.addAttribute("user", oldUser);
            model.addAttribute("userProfilePassword", "userProfilePassword");
            model.addAttribute("success", "Your password is updated successfully!");
            return "user/userPage";
        }

        model.addAttribute("user", currentUser);
        model.addAttribute("userProfilePassword", "userProfilePassword");
        return "user/userPage";
    }

    private void sendEmail() {
        // Recipient's email ID needs to be mentioned.
        String to = Common_const.sendToEmail;

        // Sender's email ID needs to be mentioned
        String from = Common_const.emailUserName;

        // Assuming you are sending email from localhost

        // Get system properties
        Properties properties = System.getProperties();

        // Setup mail server
        properties.setProperty("mail.smtp.host", Common_const.smtpHost);

        // Get the default Session object.

        properties.put("mail.smtp.starttls.enable", true);
        properties.put("mail.smtp.port", "587");
        properties.setProperty("mail.smtp.user", Common_const.emailUserName);
        properties.setProperty("mail.smtp.password", Common_const.emailPassword);
        properties.setProperty("mail.smtp.auth", "true");


        Session session = Session.getDefaultInstance(properties, new javax.mail.Authenticator() {
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(Common_const.emailUserName, Common_const.emailPassword);
            }
        });
        try {
            // Create a default MimeMessage object.
            MimeMessage message = new MimeMessage(session);

            // Set From: header field of the header.
            message.setFrom(new InternetAddress(from));

            // Set To: header field of the header.
            message.addRecipient(Message.RecipientType.TO, new InternetAddress(to));

            // Set Subject: header field
            message.setSubject("CFML FEED NEW FEED ADDED OR REFRESHED!");

            // Now set the actual message
            message.setText("DO IT RIGHT AWAY");

            // Send message
            Transport.send(message);
        } catch (Exception mex) {
            mex.printStackTrace();
        }
    }

    // ---------------- USER TRANSACTION -------------------
    @RequestMapping(value = {"/transactions"}, method = RequestMethod.GET)
    public String redirectTransactionRequest() {
        return "redirect:/user/transaction/userTransaction.xyz";
    }
    
    // display user profile
    @RequestMapping(value = "/transaction", method = RequestMethod.GET)
    public String redirectUserTransaction() {
        return "redirect:/user/transaction/userTransaction.xyz";
    }
    

    @RequestMapping(value = {"/transaction/userTransaction.xyz"}, method = RequestMethod.GET)
    public String viewUserTransaction(Model model, HttpServletRequest request) {
        if (!isAdmin(request))
            return "redirect:/index.xyz";
        UserTable user = (UserTable) request.getSession().getAttribute("currentUser");
        List<TransactionTable> trans = transactionService.getList().stream().filter(u -> u.getUserTable().getUserID() == user.getUserID())
                .collect(Collectors.toList());
        model.addAttribute("listTransaction", trans);
        return "user/userPage";
    }

}