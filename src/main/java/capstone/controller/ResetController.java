package capstone.controller;

import capstone.common.Common;
import capstone.common.Common_const;
import capstone.model.PasswordGenerator;
import capstone.model.PasswordGenerator.PasswordCharacterSet;
import capstone.model.UserTable;
import capstone.service.EntityService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.annotation.Resource;
import javax.mail.Message;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import java.security.NoSuchAlgorithmException;
import java.util.EnumSet;
import java.util.HashSet;
import java.util.List;
import java.util.Properties;
import java.util.Set;

@Controller
public class ResetController {

    @Resource(name = "usertableService")
    private EntityService<UserTable> userService;

    @RequestMapping(value = {"/reset", "reset"}, method = RequestMethod.GET)
    public String redirectRegisterRequest() {
        return "redirect:/reset.xyz";
    }

    @RequestMapping(value = "reset.xyz", method = RequestMethod.GET)
    public String register() {
        return "ResetPasswordPage";
    }

    @RequestMapping(value = "reset.xyz", method = RequestMethod.POST)
    public String resetPassword(Model model, @RequestParam(name = "inputUserName") String username,
                             @RequestParam(name = "inputEmail") String email,
                             final RedirectAttributes redirectAttributes) throws NoSuchAlgorithmException {
        
        List<UserTable> users = userService.getList();
        UserTable user = Common.INSTANCE.checkToReset(users, username, email);
		if (user == null) {
			model.addAttribute("error", "Username/Email is wrong!");
			return "ResetPasswordPage";
		}
		if (Common.INSTANCE.checkUserState(user) == 0) {
			model.addAttribute("error", "Your account is locked!");
			return "ResetPasswordPage";
		}
		Set<PasswordCharacterSet> values = new HashSet<PasswordCharacterSet>(EnumSet.allOf(SummerCharacterSets.class));
        PasswordGenerator pwGenerator = new PasswordGenerator(values, 10, 14);
        String newPassword = String.valueOf(pwGenerator.generatePassword());
        String content = "Your username: "+user.getUserName() +"\nYour new password: "+newPassword+"\nPlease use new password to login and change your password";
        String subject = "Your password is reset";
        Common.INSTANCE.sendEmail(user.getUserEmail(), subject, content);
        user.setUserPassword(Common.INSTANCE.getHashMD5(newPassword));
        userService.update(user);
        model.addAttribute("success", "Your password is reset!");
        return "LoginPage";

    }
    
    
    private static final char[] ALPHA_UPPER_CHARACTERS = { 'A', 'B', 'C', 'D',
            'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q',
            'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z' };
    private static final char[] ALPHA_LOWER_CHARACTERS = { 'a', 'b', 'c', 'd',
            'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q',
            'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z' };
    private static final char[] NUMERIC_CHARACTERS = { '0', '1', '2', '3', '4',
            '5', '6', '7', '8', '9' };
    private static final char[] SPECIAL_CHARACTERS = { '~', '`', '!', '@', '#',
            '$', '%', '^', '&', '*', '(', ')', '-', '_', '=', '+', '[', '{',
            ']', '}', '\\', '|', ';', ':', '\'', '"', ',', '<', '.', '>', '/',
            '?' };

    private enum SummerCharacterSets implements PasswordCharacterSet {
        ALPHA_UPPER(ALPHA_UPPER_CHARACTERS, 1),
        ALPHA_LOWER(ALPHA_LOWER_CHARACTERS, 1),
        NUMERIC(NUMERIC_CHARACTERS, 1),
        SPECIAL(SPECIAL_CHARACTERS, 1);

        private final char[] chars;
        private final int minUsage;

        private SummerCharacterSets(char[] chars, int minUsage) {
            this.chars = chars;
            this.minUsage = minUsage;
        }

        @Override
        public char[] getCharacters() {
            return chars;
        }

        @Override
        public int getMinCharacters() {
            return minUsage;
        }
    }
    
    
    
}
