package capstone.controller;

import capstone.common.Common;
import capstone.common.Common_const;
import capstone.model.*;
import capstone.service.EntityService;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.security.NoSuchAlgorithmException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Controller
@RequestMapping("/")
public class PriceController {

	@Resource(name = "packagetableService")
	private EntityService<PackageTable> packageService;

	@Resource(name = "subscriptiontableService")
	private EntityService<SubscriptionTable> subTable;


	@Resource(name = "usertableService")
	private EntityService<UserTable> userService;

	@Resource(name = "transactiontableService")
	private EntityService<TransactionTable> transService;

	@RequestMapping(value = { "/price", "price", }, method = RequestMethod.GET)
	public String pricePage() {
		return "redirect:/price.xyz";
	}

	@RequestMapping(value = "/price.xyz", method = RequestMethod.GET)
	public String getPackageList(Model model, HttpServletRequest request) {
        List<PackageTable> packs = packageService.getList().stream()
                .collect(Collectors.toList());
        model.addAttribute("packListNav", packs);
        model.addAttribute("packList", packs);
        return "PricePage";
    }

	// ---------------- PAYMENT -------------------

	@RequestMapping(value = "/paymentMethod")
	public String choosePaymentMethod(HttpServletRequest request, Model model) throws ParseException {
		UserTable user = (UserTable) request.getSession().getAttribute("currentUser");
		if (user == null) {
			return "LoginPage";
		}
			return "paymentProcessing/paymentMethod";
	}

	@GetMapping(value = { "/thankyou.xyz", "thankyou.xyz" })
	public String thankYou(final HttpSession session) {
		removeToken(session);
		return "paymentProcessing/paymentProcessCompleted";
	}

	@RequestMapping(value = "/paymentProcessing/choosePaymentMethod", method = RequestMethod.GET)
	public String redirectToPayment(Model model, HttpSession session, HttpServletRequest request,
			@RequestParam(value = "price") String getPrice, RedirectAttributes redirectAttributes) {
		UserTable user = (UserTable) request.getSession().getAttribute("currentUser");
		if (user == null) {
			return "LoginPage";
		}
		removeToken(session);
		int price;
		try {
			price = Integer.parseInt(getPrice);
		} catch (Exception e) {
			model.addAttribute("error", "There was an error! Please try again!");
			List<PackageTable> packs = packageService.getList().stream()
	                .collect(Collectors.toList());
	        model.addAttribute("packListNav", packs);
	        model.addAttribute("packList", packs);
	        return "PricePage";
		}
		if (!validatePriceInput(price)) {
			model.addAttribute("error", "There was an error! Please try again!");
			List<PackageTable> packs = packageService.getList().stream()
	                .collect(Collectors.toList());
	        model.addAttribute("packListNav", packs);
	        model.addAttribute("packList", packs);
	        return "PricePage";
		}
		if (session.getAttribute("currentUser") == null) {
			return "redirect:/price.xyz";
		}
		redirectAttributes.addFlashAttribute("price", price);

		return "redirect:/paymentMethod";
	}

	@GetMapping(value = "/paymentNganluong")
	public RedirectView nganluong(HttpSession session, @RequestParam(value = "price") String getPrice)
			throws NoSuchAlgorithmException {

		RedirectView redirectView = new RedirectView();
		int price;
		try {
			price = Integer.parseInt(getPrice);
		} catch (Exception e) {
			redirectView.setUrl(null);
			return redirectView;
		}
		if (!validatePriceInput(price)) {
			redirectView.setUrl(null);
			return redirectView;
		}
		if (session.getAttribute("currentUser") == null) {
			redirectView.setUrl(null);
			return redirectView;
		}
		int userID = ((UserTable) session.getAttribute("currentUser")).getUserID();
		String productName = "";
		String comment = "";
		List<PackageTable> packDetails = packageService.getList();
		for (int i = 0; i <packDetails.size(); i++) {
			if (packDetails.get(i).getPackagePrice() == price) {
				productName = packDetails.get(i).getPackageName();
				comment = packDetails.get(i).getDaysOfUsing() + " days ";
			}
		}

		String md5Price = Common.INSTANCE.getHashMD5ConfigPrice(price);
		String token = Common.INSTANCE.getHashMD5ConfigDate();

		session.setAttribute("token", Common.INSTANCE.getHashMD5CorrectDate(token));

		md5Price += userID;
		int id1 = userID % 10;
		int id2 = userID / 10;
		token = token.substring(0, 3) + id1 + token.substring(3) + id2;

		String returnURL = Common_const.RETURN_URL_SANDBOX + "/returnAfterPaymentSuccess" + "-" + md5Price + "-"
				+ token;
		redirectView.setUrl(String.format(Common_const.NGANLUONG_API_SANDBOX, Common_const.NGANLUONG_ACCOUNT_SANDBOX,
				productName, price, returnURL, comment));
		return redirectView;
	}

	@GetMapping(value = "/paymentBaoKim")
	public String baokim(Model model) {
		model.addAttribute("error", "BaoKim is not available for now!");
		List<PackageTable> packs = packageService.getList().stream()
                .collect(Collectors.toList());
        model.addAttribute("packListNav", packs);
        model.addAttribute("packList", packs);
        return "PricePage";
		// RedirectView redirectView = new RedirectView();
		//
		// int totalPrice = Integer.parseInt(Common_const.PRICE_UNLIMITED_ONE_YEAR)
		// * Integer.parseInt(Common_const.PRODUCT_QUANTITY);
		//
		// String url = String.format(Common_const.BAOKIM_API,
		// Common_const.BAOKIM_ACCOUNT, Common_const.COMMENT,
		// Common_const.PRODUCT_NAME, Common_const.PRICE_UNLIMITED_ONE_YEAR,
		// Common_const.PRODUCT_QUANTITY,
		// totalPrice, Common_const.URL_CANCEL, Common_const.URL_DETAIL,
		// Common_const.RETURN_URL);
		// redirectView.setUrl(url);
		// return redirectView;
	}

	private void removeToken(final HttpSession session) {
		if (null != session.getAttribute("token"))
			session.removeAttribute("token");
	}

	private String getCurrentDateInSession(final HttpSession session) {
		return (String) session.getAttribute("token");
	}

	private String gettokenAfterRemoveUserID(final String md5) {
		return (md5.substring(0, 3) + md5.substring(4)).substring(0, 34);
	}

	private String getUserIdFromMd5Date(final String md5) {
		return (md5.substring(0, 3) + md5.substring(4)).substring(34) + md5.charAt(3);
	}

	

	@GetMapping(value = { "/returnAfterPaymentSuccess-{md5Price}-{token}",
			"returnAfterPaymentSuccess-{md5Price}-{token}" })
	public String returnAfterPaymentSuccess(Model model, HttpSession session, @PathVariable String md5Price,
			@PathVariable String token) throws NoSuchAlgorithmException {

		if (null == session.getAttribute("token")) {
			return "redirect:/errors";
		}

		if (getCurrentDateInSession(session).isEmpty()) {
			removeToken(session);
			return "redirect:/errors";
		}

		if (!getCurrentDateInSession(session)
				.equals(Common.INSTANCE.getHashMD5CorrectPrice(gettokenAfterRemoveUserID(token)))) {
			removeToken(session);
			return "redirect:/errors";
		}

		removeToken(session);

		String userIdByMd5Date = getUserIdFromMd5Date(token);

		String md5 = Common.INSTANCE.getHashMD5CorrectPrice(md5Price);

		String userIdByMd5Price = md5.substring(32);

		md5 = md5.substring(0, 32);

		int price = 0; int count = 0;
		List<PackageTable> packDetails = packageService.getList();
		for (int i = 0; i <packDetails.size(); i++) {
			if (Common.INSTANCE.getHashMD5(String.valueOf(packDetails.get(i).getPackagePrice())).equals(md5)) {
				price = packDetails.get(i).getPackagePrice();
				count++;
			}
		}
		if (count == 0) return "redirect:/errors";

		if (Integer.parseInt(userIdByMd5Date) != Integer.parseInt(userIdByMd5Price))
			return "redirect:/errors";

		int userID = Integer.parseInt(userIdByMd5Date);

		System.out.println("Price: " + price + "\nUserID: " + userID);
		int packageID = -1;
		int subscriptionID = -1;
		for (PackageTable detail : packDetails) {
			if (detail.getPackagePrice() == price) {
				packageID = detail.getPackageID();
				break;
			}
		}
		List<SubscriptionTable> sub = subTable.getList();
		for (SubscriptionTable tab : sub) {
			if (userID == tab.getUserTable().getUserID()) {
				subscriptionID = tab.getSubscriptionID();
				break;
			}
		}
		doSubscriptionDetailsNganLuong(packageID, subscriptionID, userID);

		session.setAttribute("currentUser", userService.findById(userID));

		return "redirect:/thankyou.xyz";
	}

	private void doSubscriptionDetailsNganLuong(int planID, int subscriptionID, int userID) {
		Common.INSTANCE.updateSubscription(userService, packageService, subTable, transService, userID, planID, subscriptionID, Common_const.NGANLUONG_TRANSACTION_TYPE);

	}

	private boolean validatePriceInput(int price) {
		List<PackageTable> packs = packageService.getList().stream()
                .collect(Collectors.toList());
		for (int i = 0; i < packs.size() ; i++) {
			if (price == packs.get(i).getPackagePrice()) {
				return true;
			}
		}
		return false;
	}

}
