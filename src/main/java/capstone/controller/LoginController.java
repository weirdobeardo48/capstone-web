package capstone.controller;

import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;

import capstone.service.EntityService;
import capstone.model.UserTable;
import capstone.common.*;

@Controller
@RequestMapping("/")
@SessionAttributes("currentUser")
/**
 * Servlet implementation class Login
 */
public class LoginController {

	/**
	 * @see HttpServlet#HttpServlet()
	 */

	@Resource(name = "usertableService")
	private EntityService<UserTable> userService;

	@RequestMapping(value = { "login", "/login" }, method = RequestMethod.GET)
	public String redirectLoginRequest() {
		return "redirect:/login.xyz";
	}

	@RequestMapping(value = "login.xyz", method = RequestMethod.GET)
	public String Login(Model model, HttpServletRequest request) {
		if (request.getSession().getAttribute("currentUser") != null) {
			return "redirect:/home";
		}
		if (null != model.asMap().get("user")) {
			UserTable user = (UserTable) model.asMap().get("user");
			model.addAttribute("user", user);
		}
		return "LoginPage";
	}

	@RequestMapping(value = "/login.xyz", method = RequestMethod.POST)
	public String requestLogin(Model model, @RequestParam(name = "inputUserName", defaultValue = "") String userName,
			@RequestParam(name = "inputPassword", defaultValue = "") String password) {
		try {
			password = Common.INSTANCE.getHashMD5(password);
		}
		catch(Exception e) {}
		List<UserTable> users = userService.getList();
		UserTable user = Common.INSTANCE.checkExistUser(users, userName, password);
		if (user == null) {
			model.addAttribute("error", "Username/Password is wrong!");
			return "LoginPage";
		}
		if (Common.INSTANCE.checkUserState(user) == 0) {
			model.addAttribute("error", "Your account is locked!");
			return "LoginPage";
		}
		model.addAttribute("currentUser", user);
		if (Common.INSTANCE.isAdmin(user)) {
			return "redirect:/admin/";
		}
		if (Common.INSTANCE.isRoot(user)) {
			return "redirect:/root/";
		}
		return "redirect:/home";
	}

}
