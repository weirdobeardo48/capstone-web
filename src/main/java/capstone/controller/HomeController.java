package capstone.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;

import capstone.model.Application_Version;
import capstone.model.UserTable;
import capstone.service.EntityService;

import java.io.IOException;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

@Controller
public class HomeController {
	@Resource(name = "applicationversionService")
	 private EntityService<Application_Version> appService;

	 @RequestMapping(value = {"", "index", "home", "/", "/index","/home"}, method = RequestMethod.GET)
	    public String homePage() {
	        return "redirect:/index.xyz";
	    }

	    @RequestMapping(value = "/index.xyz", method = RequestMethod.GET)
	    public String home(Model model, HttpServletRequest request)  throws IOException{
		    	List<Application_Version> list = appService.getList();
				String link = list.get(list.size()-1).getAppLink();
				model.addAttribute("link",link);
	            return "index";
	    }

	    @RequestMapping(value = {"panelPage.xyz", "/panelPage.xyz"}, method = RequestMethod.GET)
	    public String redirectToCorrectPanel(HttpServletRequest request){
	        if(request.getSession().getAttribute("currentUser") == null){
	            return "redirect:/index.xyz";
	        }
	        UserTable user = (UserTable) request.getSession().getAttribute("currentUser");
	        if (user.getUserType() == 2) return "redirect:/admin";
	        else if (user.getUserType() == 0) return "redirect:/root";
	        else return "redirect:/user";
	    }
}
