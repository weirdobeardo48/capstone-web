package capstone.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the transactiontable database table.
 * 
 */
@Entity
@Table(name = "transactionTable")
@NamedQuery(name="TransactionTable.findAll", query="SELECT t FROM TransactionTable t")
public class TransactionTable implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private int transactionID;

	@Temporal(TemporalType.DATE)
	private Date transactionnDate;

	private int transactionPackageDays;

	private String transactionPackageName;

	private int transactionPackagePrice;

	private int transactionType;

	//bi-directional many-to-one association to Usertable
	@ManyToOne
	@JoinColumn(name="userID")
	private UserTable UserTable;

	public TransactionTable() {
	}

	public int getTransactionID() {
		return this.transactionID;
	}

	public void setTransactionID(int transactionID) {
		this.transactionID = transactionID;
	}

	public Date getTransactionnDate() {
		return this.transactionnDate;
	}

	public void setTransactionnDate(Date transactionnDate) {
		this.transactionnDate = transactionnDate;
	}

	public int getTransactionPackageDays() {
		return this.transactionPackageDays;
	}

	public void setTransactionPackageDays(int transactionPackageDays) {
		this.transactionPackageDays = transactionPackageDays;
	}

	public String getTransactionPackageName() {
		return this.transactionPackageName;
	}

	public void setTransactionPackageName(String transactionPackageName) {
		this.transactionPackageName = transactionPackageName;
	}

	public int getTransactionPackagePrice() {
		return this.transactionPackagePrice;
	}

	public void setTransactionPackagePrice(int transactionPackagePrice) {
		this.transactionPackagePrice = transactionPackagePrice;
	}

	public int getTransactionType() {
		return this.transactionType;
	}

	public void setTransactionType(int transactionType) {
		this.transactionType = transactionType;
	}

	public UserTable getUserTable() {
		return this.UserTable;
	}

	public void setUserTable(UserTable UserTable) {
		this.UserTable = UserTable;
	}

}