package capstone.model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the Data_Version database table.
 * 
 */
@Entity
@NamedQuery(name="Data_Version.findAll", query="SELECT d FROM Data_Version d")
public class Data_Version implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int dataVersionID;

	private String dataVersionNumber;

	public Data_Version() {
	}

	public int getDataVersionID() {
		return this.dataVersionID;
	}

	public void setDataVersionID(int dataVersionID) {
		this.dataVersionID = dataVersionID;
	}

	public String getDataVersionNumber() {
		return this.dataVersionNumber;
	}

	public void setDataVersionNumber(String dataVersionNumber) {
		this.dataVersionNumber = dataVersionNumber;
	}

}