package capstone.model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the Application_Version database table.
 * 
 */
@Entity
@Table(name="Application_Version")
@NamedQuery(name="Application_Version.findAll", query="SELECT a FROM Application_Version a")
public class Application_Version implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(unique=true, nullable=false)
	private int appVersionID;

	@Lob
	private String appLink;

	@Column(nullable=false, length=10)
	private String appVersionNumber;

	@Lob
	private String changeLog;

	public Application_Version() {
	}

	public int getAppVersionID() {
		return this.appVersionID;
	}

	public void setAppVersionID(int appVersionID) {
		this.appVersionID = appVersionID;
	}

	public String getAppLink() {
		return this.appLink;
	}

	public void setAppLink(String appLink) {
		this.appLink = appLink;
	}

	public String getAppVersionNumber() {
		return this.appVersionNumber;
	}

	public void setAppVersionNumber(String appVersionNumber) {
		this.appVersionNumber = appVersionNumber;
	}

	public String getChangeLog() {
		return this.changeLog;
	}

	public void setChangeLog(String changeLog) {
		this.changeLog = changeLog;
	}

}