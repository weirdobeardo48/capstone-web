package capstone.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.util.Calendar;

/**
 * The persistent class for the subscriptionTable database table.
 * 
 */
@Entity
@Table(name = "subscriptionTable")
@NamedQuery(name = "SubscriptionTable.findAll", query = "SELECT s FROM SubscriptionTable s")
public class SubscriptionTable implements Serializable {
	private static final long serialVersionUID = 1L;
	private int subscriptionID;
	private Date expiredDate;
	private byte isExpired;
	private UserTable userTable;

	public SubscriptionTable() {
		Calendar calendar = Calendar.getInstance();
		calendar.add(Calendar.DATE, -1);

		Date date = new Date(calendar.getTimeInMillis());
		setExpiredDate(date);
	}

	@Id
	@Column(unique = true, nullable = false)
	public int getSubscriptionID() {
		return this.subscriptionID;
	}

	public void setSubscriptionID(int subscriptionID) {
		this.subscriptionID = subscriptionID;
	}

	@Temporal(TemporalType.DATE)
	@Column(nullable = false)
	public Date getExpiredDate() {
		return this.expiredDate;
	}

	public void setExpiredDate(Date expiredDate) {
		this.expiredDate = expiredDate;
	}

	@Column(nullable = false)
	public byte getIsExpired() {
		return this.isExpired;
	}

	public void setIsExpired(byte isExpired) {
		this.isExpired = isExpired;
	}

	// bi-directional one-to-one association to UserTable
	@OneToOne
	@JoinColumn(name = "userID")
	public UserTable getUserTable() {
		return this.userTable;
	}

	public void setUserTable(UserTable userTable) {
		this.userTable = userTable;
	}

}