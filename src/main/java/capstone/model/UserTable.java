package capstone.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;

/**
 * The persistent class for the userTable database table.
 * 
 */
@Entity
@Table(name = "userTable")
@NamedQuery(name = "UserTable.findAll", query = "SELECT u FROM UserTable u")
public class UserTable {

	@Id
	@Column(unique = true, nullable = false)
	private int userID;

	private byte isActive;

	@Column(length = 100)
	private String userEmail;

	@Column(nullable = false, length = 100)
	private String userName;

	@Column(nullable = false, length = 100)
	private String userPassword;

	@Column(length = 20)
	private String userPhone;

	@Column(nullable = false)
	private int userType;

	// bi-directional one-to-one association to SubscriptionTable
	@OneToOne(mappedBy = "userTable")
	private SubscriptionTable subscriptionTable;

	public UserTable() {
	}
	public UserTable(int id, String name, String password, String email, String phone, int type) {
		userID = id;
		userName = name;
		userPassword = password;
		userEmail = email;
		userPhone = phone;
		userType = type;
	}
	public int getUserID() {
		return this.userID;
	}

	public void setUserID(int userID) {
		this.userID = userID;
	}

	public byte getIsActive() {
		return this.isActive;
	}

	public void setIsActive(byte isActive) {
		this.isActive = isActive;
	}

	public String getUserEmail() {
		return this.userEmail;
	}

	public void setUserEmail(String userEmail) {
		this.userEmail = userEmail;
	}

	public String getUserName() {
		return this.userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getUserPassword() {
		return this.userPassword;
	}

	public void setUserPassword(String userPassword) {
		this.userPassword = userPassword;
	}

	public String getUserPhone() {
		return this.userPhone;
	}

	public void setUserPhone(String userPhone) {
		this.userPhone = userPhone;
	}

	public int getUserType() {
		return this.userType;
	}

	public void setUserType(int userType) {
		this.userType = userType;
	}

	public SubscriptionTable getSubscriptionTable() {
		return this.subscriptionTable;
	}

	public void setSubscriptionTable(SubscriptionTable subscriptionTable) {
		this.subscriptionTable = subscriptionTable;
	}

	@OneToMany
	@JoinColumn(name = "userID", referencedColumnName = "userID", nullable = false, insertable = false, updatable = false)
	private List<TransactionTable> transactionTable;

	public List<TransactionTable> getTransactionTable() {
		return this.transactionTable;
	}

	@Override
	public int hashCode() {
		int result = userID;
		result = 31 * result + (userName != null ? userName.hashCode() : 0);
		result = 31 * result + userType;
		return result;
	}
	
}