package capstone.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the newsTable database table.
 * 
 */
@Entity
@Table(name="newsTable")
@NamedQuery(name="NewsTable.findAll", query="SELECT n FROM NewsTable n")
public class NewsTable implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int newsID;

	private String author;

	@Lob
	private String content;

	@Temporal(TemporalType.DATE)
	private Date publishedDate;

	private String title;

	public NewsTable() {
	}

	public int getNewsID() {
		return this.newsID;
	}

	public void setNewsID(int newsID) {
		this.newsID = newsID;
	}

	public String getAuthor() {
		return this.author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public String getContent() {
		return this.content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public Date getPublishedDate() {
		return this.publishedDate;
	}

	public void setPublishedDate(Date publishedDate) {
		this.publishedDate = publishedDate;
	}

	public String getTitle() {
		return this.title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

}