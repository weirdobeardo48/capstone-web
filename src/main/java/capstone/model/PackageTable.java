package capstone.model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the packagetable database table.
 * 
 */
@Entity
@Table(name = "packageTable")
@NamedQuery(name="PackageTable.findAll", query="SELECT p FROM PackageTable p")
public class PackageTable implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private int packageID;

	private int daysOfUsing;

	private String packageDescription;

	private String packageName;

	private int packagePrice;

	public PackageTable() {
	}

	public int getPackageID() {
		return this.packageID;
	}

	public void setPackageID(int packageID) {
		this.packageID = packageID;
	}

	public int getDaysOfUsing() {
		return this.daysOfUsing;
	}

	public void setDaysOfUsing(int daysOfUsing) {
		this.daysOfUsing = daysOfUsing;
	}

	public String getPackageDescription() {
		return this.packageDescription;
	}

	public void setPackageDescription(String packageDescription) {
		this.packageDescription = packageDescription;
	}

	public String getPackageName() {
		return this.packageName;
	}

	public void setPackageName(String packageName) {
		this.packageName = packageName;
	}

	public int getPackagePrice() {
		return this.packagePrice;
	}

	public void setPackagePrice(int packagePrice) {
		this.packagePrice = packagePrice;
	}
	 @Override
	    public int hashCode() {
	        int result = packageID;
	        result = 31 * result + (packageName != null ? packageName.hashCode() : 0);
	        result = 31 * result + (packageDescription != null ? packageDescription.hashCode() : 0);
	        return result;
	    }

}