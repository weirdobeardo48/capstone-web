package capstone.service.interfaces;

import capstone.dao.EntityDao;
import capstone.model.PackageTable;
import capstone.service.EntityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service("packagetableService")
@Transactional
public class PackageTableServiceImpl implements EntityService<PackageTable> {

    @Autowired
    EntityDao<PackageTable> dao;

    public PackageTable findById(int id) {
        return dao.findById(id);
    }

    public void save(PackageTable entity) {
        dao.save(entity);
    }

    public void update(PackageTable entity) {
        dao.update(entity);
    }

    public void delete(PackageTable entity) {
        dao.remove(entity);
    }

    public List<PackageTable> getList() {
        return dao.getList();
    }
}
