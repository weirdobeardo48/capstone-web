package capstone.service.interfaces;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import capstone.dao.EntityDao;
import capstone.model.Application_Version;
import capstone.service.EntityService;

@Service("applicationversionService")
@Transactional
public class Application_VersionServiceImpl implements EntityService<Application_Version> {

    @Autowired
    EntityDao<Application_Version> dao;

    public Application_Version findById(int id) {
        return dao.findById(id);
    }

    public void save(Application_Version entity) {
        dao.save(entity);
    }

    public void update(Application_Version entity) {
        dao.update(entity);
    }

    public void delete(Application_Version entity) {
        dao.remove(entity);
    }

    public List<Application_Version> getList() {
        return dao.getList();
    }
}
