package capstone.service.interfaces;

import capstone.dao.EntityDao;
import capstone.model.TransactionTable;
import capstone.service.EntityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service("transactiontableService")
@Transactional
public class TransactionTableServiceImpl implements EntityService<TransactionTable> {

    @Autowired
    EntityDao<TransactionTable> dao;

    public TransactionTable findById(int id) {
        return dao.findById(id);
    }

    public void save(TransactionTable entity) {
        dao.save(entity);
    }

    public void update(TransactionTable entity) {
        dao.update(entity);
    }

    public void delete(TransactionTable entity) {
        dao.remove(entity);
    }

    public List<TransactionTable> getList() {
        return dao.getList();
    }
}
