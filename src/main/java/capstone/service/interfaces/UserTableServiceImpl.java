package capstone.service.interfaces;

import capstone.dao.EntityDao;
import capstone.model.UserTable;
import capstone.service.EntityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service("usertableService")
@Transactional
public class UserTableServiceImpl implements EntityService<UserTable> {

    @Autowired
    EntityDao<UserTable> dao;

    public UserTable findById(int id) {
        return dao.findById(id);
    }

    public void save(UserTable entity) {
        dao.save(entity);
    }

    public void update(UserTable entity) {
        dao.update(entity);
    }

    public void delete(UserTable entity) {
        dao.remove(entity);
    }

    public List<UserTable> getList() {
        return dao.getList();
    }
}
