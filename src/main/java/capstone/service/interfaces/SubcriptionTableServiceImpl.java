package capstone.service.interfaces;

import capstone.dao.EntityDao;
import capstone.model.SubscriptionTable;
import capstone.service.EntityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service("subscriptiontableService")
@Transactional
public class SubcriptionTableServiceImpl implements EntityService<SubscriptionTable> {

    private final EntityDao<SubscriptionTable> dao;

    @Autowired
    public SubcriptionTableServiceImpl(EntityDao<SubscriptionTable> dao) {
        this.dao = dao;
    }

    public SubscriptionTable findById(int id) {
        return dao.findById(id);
    }

    public void save(SubscriptionTable entity) {
        dao.save(entity);
    }

    public void update(SubscriptionTable entity) {
        dao.update(entity);
    }

    public void delete(SubscriptionTable entity) {
        dao.remove(entity);
    }

    public List<SubscriptionTable> getList() {
        return dao.getList();
    }
}
