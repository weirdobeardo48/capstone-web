package capstone.service;
import java.util.List;
public interface EntityService<T> {
    T findById(int id);

    void save(T entity);

    void update(T entity);

    void delete(T entity);

    List<T> getList();
}

