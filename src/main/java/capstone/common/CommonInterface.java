package capstone.common;

import java.util.Map;

public interface CommonInterface {
    int getProductCount(String shopUrl, String shopApiKey, String password);
    String getDomain(String shopUrl, String shopApiKey, String password);
    Map<String, String> getAllCollection(String shopUrl, String shopApiKey, String password);
}
