package capstone.common;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.codec.binary.Base64;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

public class common_SPF implements CommonInterface{

	public common_SPF() {
	}

	public int getProductCount(String shopUrl, String shopApiKey, String password) {
		if (!shopUrl.startsWith("https://")) {
			shopUrl = "https://" + shopUrl;
		}

		String urlToFetch = shopUrl + "/admin/products/count.json";
		String a = getContent(urlToFetch, shopApiKey, password);
		if (a == null) {
			return -1;
		} else {
			int indexToCut = a.indexOf(":");
			a = a.substring(indexToCut + 1, a.length() - 1);
			System.out.println(a);
			try {
				return Integer.parseInt(a);
			} catch (Exception e) {
				return -1;
			}
		}
	}

	public String getDomain(String shopUrl, String shopApiKey, String password) {

		if (!shopUrl.startsWith("https://")) {
			shopUrl = "https://" + shopUrl;
		}
		String urlToFetch = shopUrl + "/admin/shop.json";
		String result = getContent(urlToFetch, shopApiKey, password);
		if (result == null) {
			return null;
		}
		if (!result.contains("domain")) {
			return null;
		} else {
			int indexToCut = result.indexOf("\"domain\"") + 9;
			result = result.substring(indexToCut, result.length() - indexToCut);
			result = result.substring(0, result.indexOf(","));
			result = result.replace("\"", "");

			return result;
		}
	}

	public Map<String, String> getAllCollection(String shopUrl, String shopApiKey, String password) {
		JSONParser parser = new JSONParser();
		if (!shopUrl.startsWith("https://")) {
			shopUrl = "https://" + shopUrl;
		}

		HashMap<String, String> collection = new HashMap<>();
		int customCollectionCount = getCustomCollectionCount(shopUrl, shopApiKey, password);
		int smartCollectionCount = getSmartCollectionCount(shopUrl, shopApiKey, password);
		int numberOfPageCustomCollection = 0;
		int numberOfPageSmartCollection = 0;

		if (customCollectionCount != -1) {
			if (customCollectionCount % 250 != 0) {
				numberOfPageCustomCollection = customCollectionCount / 250 + 1;
			} else {
				numberOfPageCustomCollection = customCollectionCount / 250;
			}
		}

		if (smartCollectionCount != -1) {

			if (smartCollectionCount % 250 != 0) {
				numberOfPageSmartCollection = smartCollectionCount / 250 + 1;
			} else {
				numberOfPageSmartCollection = smartCollectionCount / 250;
			}
		}

		for (int i = 1; i <= numberOfPageCustomCollection; i++) {
			String urlToGetProductJson = shopUrl + "/admin/custom_collections.json?limit=250&page=" + i;
			try {
				String result = getContent(urlToGetProductJson, shopApiKey, password);

				Object obj = parser.parse(result);
				JSONObject jsonObject = (JSONObject) obj;
				JSONArray customCollection = (JSONArray) jsonObject.get("custom_collections");

				System.out.println("Bat dau in:");
				for (int j = 0; j < customCollection.size(); j++) {
					JSONObject ob = (JSONObject) customCollection.get(j);
					collection.put(ob.get("id").toString(), ob.get("title").toString());
				}
			} catch (Exception e) {

			}

		}

		for (int i = 1; i <= numberOfPageSmartCollection; i++) {
			String urlToGetProductJson = shopUrl + "/admin/smart_collections.json?limit=250&page=" + i;
			try {
				String result = getContent(urlToGetProductJson, shopApiKey, password);

				Object obj = parser.parse(result);
				JSONObject jsonObject = (JSONObject) obj;
				JSONArray smartCollection = (JSONArray) jsonObject.get("smart_collections");

				System.out.println("Bat dau in:");
				for (int j = 0; j < smartCollection.size(); j++) {
					JSONObject ob = (JSONObject) smartCollection.get(j);
					collection.put(ob.get("id").toString(), ob.get("title").toString());
				}
			} catch (Exception e) {
				e.printStackTrace();
			}

		}

		return collection;
	}

	public int getCustomCollectionCount(String shopUrl, String shopApiKey, String password) {
		if (!shopUrl.startsWith("https://")) {
			shopUrl = "https://" + shopUrl;
		}
		try {
			String urlToFetch = shopUrl + "/admin/custom_collections/count.json";
			String result = getContent(urlToFetch, shopApiKey, password);
			int indexToCut = result.indexOf(":") + 1;
			result = result.substring(indexToCut, result.length() );
			result = result.substring(0, result.indexOf("}"));
			return Integer.parseInt(result);
		} catch (Exception e) {
			return -1;
		}
	}

	public int getSmartCollectionCount(String shopUrl, String shopApiKey, String password) {
		if (!shopUrl.startsWith("https://")) {
			shopUrl = "https://" + shopUrl;
		}
		try {
			String urlToFetch = shopUrl + "/admin/smart_collections/count.json";
			String result = getContent(urlToFetch, shopApiKey, password);
			int indexToCut = result.indexOf(":") + 1;
			result = result.substring(indexToCut, result.length());
			result = result.substring(0, result.indexOf("}"));
			return Integer.parseInt(result);
		} catch (Exception e) {
			return -1;
		}
	}

	public String getContent(String urlToFetch, String shopApiKey, String password) {

		// System.setProperty("https.protocols", "TLSv1.1");

		String body = "";
		try {

			DefaultHttpClient httpClient = new DefaultHttpClient();
			HttpGet getRequest = new HttpGet(urlToFetch);
			String userPass = shopApiKey + ":" + password;
			String encoded = new String(new Base64().encode(userPass.getBytes()));
			getRequest.addHeader("Authorization", "Basic " + encoded);
			// getRequest.addHeader("Accept",
			// "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8");
			// getRequest.addHeader("Connection", "keep-alive");
			// getRequest.addHeader("User-Agent",
			// "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:57.0) Gecko/20100101
			// Firefox/57.0");
			// getRequest.addHeader("Accept-Encoding", "gzip, deflate, br");
			// getRequest.addHeader("Accept-Language", "en-US,en;q=0.5");
			HttpResponse response = httpClient.execute(getRequest);
			body = EntityUtils.toString(response.getEntity());
			System.out.println(body);
			System.out.println("Done");

		} catch (Exception e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
			return null;
		}
		return body;
	}

}