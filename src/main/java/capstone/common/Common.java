package capstone.common;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.regex.Pattern;

import javax.mail.Message;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import capstone.model.PackageTable;
import capstone.model.SubscriptionTable;
import capstone.model.TransactionTable;
import capstone.model.UserTable;

import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import capstone.service.EntityService;

public enum Common {

	INSTANCE;

	public byte checkUserState(UserTable user) {
		return user.getIsActive();
	}

	public boolean isAdmin(UserTable user) {
		return user.getUserType() == Common_const.isADMIN;
	}

	public boolean isRoot(UserTable user) {
		return user.getUserType() == Common_const.isROOT;
	}
	
	public UserTable checkExistUser(List<UserTable> users, final String username, final String password) {
		UserTable usertableEntity = null;
		for (UserTable user : users) {
			if (user.getUserPassword().equals(password) && user.getUserName().equalsIgnoreCase(username)) {
				usertableEntity = user;
				break;
			}
		}
		return usertableEntity;
	}
	
	public UserTable checkToReset(List<UserTable> users, final String username, final String email) {
		UserTable usertableEntity = null;
		for (UserTable user : users) {
			if (user.getUserEmail().equals(email) && user.getUserName().equalsIgnoreCase(username)) {
				usertableEntity = user;
				break;
			}
		}
		return usertableEntity;
	}

	public int getRandomNumber() {
		Random random = new Random();
		return random.nextInt(9);
	}

	public String getHashMD5(String plaintext) throws NoSuchAlgorithmException {
		MessageDigest m = MessageDigest.getInstance("MD5");
		m.reset();
		m.update(plaintext.getBytes());
		byte[] digest = m.digest();
		BigInteger bigInt = new BigInteger(1, digest);
		StringBuilder hashtext = new StringBuilder(bigInt.toString(16));
		// Now we need to zero pad it if you actually want the full 32 chars.
		while (hashtext.length() < 32) {
			hashtext.insert(0, "0");
		}
		return hashtext.toString().toUpperCase();
	}

	public String getHashMD5ConfigPrice(int price) throws NoSuchAlgorithmException {
		String md5Price = getHashMD5(price + "");

		int index = getRandomNumber();

		int otherIndex = index * 3;
		otherIndex %= 4;

		md5Price = md5Price.substring(0, 5) + index + md5Price.substring(5, (index + 10)) + otherIndex
				+ md5Price.substring((index + 10));
		return md5Price;
	}

	public String getHashMD5CorrectPrice(final String string) {
		int index = Integer.parseInt(string.charAt(5) + "");
		String md5 = string.substring(0, 5) + string.substring(6);
		md5 = md5.substring(0, index + 10) + md5.substring(index + 11);
		return md5;
	}

	public String getHashMD5ConfigDate() throws NoSuchAlgorithmException {
		String md5 = getHashMD5(Calendar.getInstance().getTime().toString());
		int index = getRandomNumber();

		int otherIndex = index * 3;
		otherIndex %= 4;

		md5 = md5.substring(0, 5) + index + md5.substring(5, (index + 10)) + otherIndex + md5.substring((index + 10));

		return md5;
	}

	public String getHashMD5CorrectDate(final String string) {
		int index = Integer.parseInt(string.charAt(5) + "");
		String md5 = string.substring(0, 5) + string.substring(6);
		md5 = md5.substring(0, index + 10) + md5.substring(index + 11);
		return md5;
	}


	private static final List<String> categories;

	static {
		categories = new ArrayList<>();
		Resource resource = new ClassPathResource("ggcategory.txt");
		InputStream is = null;
		try {
			is = resource.getInputStream();

			BufferedReader br = new BufferedReader(new InputStreamReader(is));

			String line;
			while ((line = br.readLine()) != null) {
				String[] texts = line.split("[-]");
				if (texts.length == 2) {
					try {
						Integer.parseInt(texts[0].trim());
						categories.add(texts[1].trim());
					} catch (NumberFormatException e) {
					}
				}
			}

			is.close();
		} catch (IOException e) {
			if (is != null) {
				try {
					is.close();
				} catch (IOException e1) {
					e1.printStackTrace();
				}
			}
		}
	}

	public List<String> getCategories() {
		return categories;
	}

	// ---CheckValidData

	public boolean checkExistUsername(List<UserTable> users, UserTable user) {
		for (UserTable u : users) {
			if (user.getUserName().equals(u.getUserName())) {
				return false;
			}
		}
		return true;
	}

	public boolean checkExistEmail(List<UserTable> users, UserTable user) {
		for (UserTable u : users) {
			if (user.getUserEmail().equals(u.getUserEmail())) {
				return false;
			}
		}
		return true;
	}

	
	public boolean checkExistPhone(List<UserTable> users, UserTable user) {
		for (UserTable u : users) {
			if (user.getUserPhone().equals(u.getUserPhone())) {
				return false;
			}
		}
		return true;
	}
	
	public boolean checkEmailForUpdate(List<UserTable> users, UserTable user) {
		for (UserTable u : users) {
			if (user.getUserEmail().equals(u.getUserEmail()) && user.getUserID() != u.getUserID()) {
				return false;
			}
		}
		return true;
	}
	
	public boolean checkPhoneForUpdate(List<UserTable> users, UserTable user) {
		for (UserTable u : users) {
			if (user.getUserPhone().equals(u.getUserPhone()) && u.getUserID() != user.getUserID()) {
				return false;
			}
		}
		return true;
	}
	
	public boolean checkEmpty(String s) {
		if (s == null || s.trim().isEmpty()) return false;
		return true;
	}
	
	public boolean checkMinLength(String s) {
		if (s.length() < 8) return false;
		return true;
	}
	
	public boolean checkMaxLength(String s) {
		if (s.length() > 16) return false;
		return true;
	}
	
	public boolean checkValidUsername(String s) {
		Pattern username = Pattern.compile("[a-zA-Z0-9]*");
		return username.matcher(s).matches();
	}
	public boolean checkValidEmail(String s) {
		Pattern email = Pattern.compile(
				"(?:(?:\\r\\n)?[ \\t])*(?:(?:(?:[^()<>@,;:\\\\\".\\[\\] \\000-\\031]+(?:(?:(?:\\r\\n)?[ \\t])+|\\Z|(?=[\\[\"()<>@,;:\\\\\".\\[\\]]))|\"(?:[^\\\"\\r\\\\]|\\\\.|(?:(?:\\r\\n)?[ \\t]))*\"(?:(?:\\r\\n)?[ \\t])*)(?:\\.(?:(?:\\r\\n)?[ \\t])*(?:[^()<>@,;:\\\\\".\\[\\] \\000-\\031]+(?:(?:(?:\\r\\n)?[ \\t])+|\\Z|(?=[\\[\"()<>@,;:\\\\\".\\[\\]]))|\"(?:[^\\\"\\r\\\\]|\\\\.|(?:(?:\\r\\n)?[ \\t]))*\"(?:(?:\\r\\n)?[ \\t])*))*@(?:(?:\\r\\n)?[ \\t])*(?:[^()<>@,;:\\\\\".\\[\\] \\000-\\031]+(?:(?:(?:\\r\\n)?[ \\t])+|\\Z|(?=[\\[\"()<>@,;:\\\\\".\\[\\]]))|\\[([^\\[\\]\\r\\\\]|\\\\.)*\\](?:(?:\\r\\n)?[ \\t])*)(?:\\.(?:(?:\\r\\n)?[ \\t])*(?:[^()<>@,;:\\\\\".\\[\\] \\000-\\031]+(?:(?:(?:\\r\\n)?[ \\t])+|\\Z|(?=[\\[\"()<>@,;:\\\\\".\\[\\]]))|\\[([^\\[\\]\\r\\\\]|\\\\.)*\\](?:(?:\\r\\n)?[ \\t])*))*|(?:[^()<>@,;:\\\\\".\\[\\] \\000-\\031]+(?:(?:(?:\\r\\n)?[ \\t])+|\\Z|(?=[\\[\"()<>@,;:\\\\\".\\[\\]]))|\"(?:[^\\\"\\r\\\\]|\\\\.|(?:(?:\\r\\n)?[ \\t]))*\"(?:(?:\\r\\n)?[ \\t])*)*\\<(?:(?:\\r\\n)?[ \\t])*(?:@(?:[^()<>@,;:\\\\\".\\[\\] \\000-\\031]+(?:(?:(?:\\r\\n)?[ \\t])+|\\Z|(?=[\\[\"()<>@,;:\\\\\".\\[\\]]))|\\[([^\\[\\]\\r\\\\]|\\\\.)*\\](?:(?:\\r\\n)?[ \\t])*)(?:\\.(?:(?:\\r\\n)?[ \\t])*(?:[^()<>@,;:\\\\\".\\[\\] \\000-\\031]+(?:(?:(?:\\r\\n)?[ \\t])+|\\Z|(?=[\\[\"()<>@,;:\\\\\".\\[\\]]))|\\[([^\\[\\]\\r\\\\]|\\\\.)*\\](?:(?:\\r\\n)?[ \\t])*))*(?:,@(?:(?:\\r\\n)?[ \\t])*(?:[^()<>@,;:\\\\\".\\[\\] \\000-\\031]+(?:(?:(?:\\r\\n)?[ \\t])+|\\Z|(?=[\\[\"()<>@,;:\\\\\".\\[\\]]))|\\[([^\\[\\]\\r\\\\]|\\\\.)*\\](?:(?:\\r\\n)?[ \\t])*)(?:\\.(?:(?:\\r\\n)?[ \\t])*(?:[^()<>@,;:\\\\\".\\[\\] \\000-\\031]+(?:(?:(?:\\r\\n)?[ \\t])+|\\Z|(?=[\\[\"()<>@,;:\\\\\".\\[\\]]))|\\[([^\\[\\]\\r\\\\]|\\\\.)*\\](?:(?:\\r\\n)?[ \\t])*))*)*:(?:(?:\\r\\n)?[ \\t])*)?(?:[^()<>@,;:\\\\\".\\[\\] \\000-\\031]+(?:(?:(?:\\r\\n)?[ \\t])+|\\Z|(?=[\\[\"()<>@,;:\\\\\".\\[\\]]))|\"(?:[^\\\"\\r\\\\]|\\\\.|(?:(?:\\r\\n)?[ \\t]))*\"(?:(?:\\r\\n)?[ \\t])*)(?:\\.(?:(?:\\r\\n)?[ \\t])*(?:[^()<>@,;:\\\\\".\\[\\] \\000-\\031]+(?:(?:(?:\\r\\n)?[ \\t])+|\\Z|(?=[\\[\"()<>@,;:\\\\\".\\[\\]]))|\"(?:[^\\\"\\r\\\\]|\\\\.|(?:(?:\\r\\n)?[ \\t]))*\"(?:(?:\\r\\n)?[ \\t])*))*@(?:(?:\\r\\n)?[ \\t])*(?:[^()<>@,;:\\\\\".\\[\\] \\000-\\031]+(?:(?:(?:\\r\\n)?[ \\t])+|\\Z|(?=[\\[\"()<>@,;:\\\\\".\\[\\]]))|\\[([^\\[\\]\\r\\\\]|\\\\.)*\\](?:(?:\\r\\n)?[ \\t])*)(?:\\.(?:(?:\\r\\n)?[ \\t])*(?:[^()<>@,;:\\\\\".\\[\\] \\000-\\031]+(?:(?:(?:\\r\\n)?[ \\t])+|\\Z|(?=[\\[\"()<>@,;:\\\\\".\\[\\]]))|\\[([^\\[\\]\\r\\\\]|\\\\.)*\\](?:(?:\\r\\n)?[ \\t])*))*\\>(?:(?:\\r\\n)?[ \\t])*)|(?:[^()<>@,;:\\\\\".\\[\\] \\000-\\031]+(?:(?:(?:\\r\\n)?[ \\t])+|\\Z|(?=[\\[\"()<>@,;:\\\\\".\\[\\]]))|\"(?:[^\\\"\\r\\\\]|\\\\.|(?:(?:\\r\\n)?[ \\t]))*\"(?:(?:\\r\\n)?[ \\t])*)*:(?:(?:\\r\\n)?[ \\t])*(?:(?:(?:[^()<>@,;:\\\\\".\\[\\] \\000-\\031]+(?:(?:(?:\\r\\n)?[ \\t])+|\\Z|(?=[\\[\"()<>@,;:\\\\\".\\[\\]]))|\"(?:[^\\\"\\r\\\\]|\\\\.|(?:(?:\\r\\n)?[ \\t]))*\"(?:(?:\\r\\n)?[ \\t])*)(?:\\.(?:(?:\\r\\n)?[ \\t])*(?:[^()<>@,;:\\\\\".\\[\\] \\000-\\031]+(?:(?:(?:\\r\\n)?[ \\t])+|\\Z|(?=[\\[\"()<>@,;:\\\\\".\\[\\]]))|\"(?:[^\\\"\\r\\\\]|\\\\.|(?:(?:\\r\\n)?[ \\t]))*\"(?:(?:\\r\\n)?[ \\t])*))*@(?:(?:\\r\\n)?[ \\t])*(?:[^()<>@,;:\\\\\".\\[\\] \\000-\\031]+(?:(?:(?:\\r\\n)?[ \\t])+|\\Z|(?=[\\[\"()<>@,;:\\\\\".\\[\\]]))|\\[([^\\[\\]\\r\\\\]|\\\\.)*\\](?:(?:\\r\\n)?[ \\t])*)(?:\\.(?:(?:\\r\\n)?[ \\t])*(?:[^()<>@,;:\\\\\".\\[\\] \\000-\\031]+(?:(?:(?:\\r\\n)?[ \\t])+|\\Z|(?=[\\[\"()<>@,;:\\\\\".\\[\\]]))|\\[([^\\[\\]\\r\\\\]|\\\\.)*\\](?:(?:\\r\\n)?[ \\t])*))*|(?:[^()<>@,;:\\\\\".\\[\\] \\000-\\031]+(?:(?:(?:\\r\\n)?[ \\t])+|\\Z|(?=[\\[\"()<>@,;:\\\\\".\\[\\]]))|\"(?:[^\\\"\\r\\\\]|\\\\.|(?:(?:\\r\\n)?[ \\t]))*\"(?:(?:\\r\\n)?[ \\t])*)*\\<(?:(?:\\r\\n)?[ \\t])*(?:@(?:[^()<>@,;:\\\\\".\\[\\] \\000-\\031]+(?:(?:(?:\\r\\n)?[ \\t])+|\\Z|(?=[\\[\"()<>@,;:\\\\\".\\[\\]]))|\\[([^\\[\\]\\r\\\\]|\\\\.)*\\](?:(?:\\r\\n)?[ \\t])*)(?:\\.(?:(?:\\r\\n)?[ \\t])*(?:[^()<>@,;:\\\\\".\\[\\] \\000-\\031]+(?:(?:(?:\\r\\n)?[ \\t])+|\\Z|(?=[\\[\"()<>@,;:\\\\\".\\[\\]]))|\\[([^\\[\\]\\r\\\\]|\\\\.)*\\](?:(?:\\r\\n)?[ \\t])*))*(?:,@(?:(?:\\r\\n)?[ \\t])*(?:[^()<>@,;:\\\\\".\\[\\] \\000-\\031]+(?:(?:(?:\\r\\n)?[ \\t])+|\\Z|(?=[\\[\"()<>@,;:\\\\\".\\[\\]]))|\\[([^\\[\\]\\r\\\\]|\\\\.)*\\](?:(?:\\r\\n)?[ \\t])*)(?:\\.(?:(?:\\r\\n)?[ \\t])*(?:[^()<>@,;:\\\\\".\\[\\] \\000-\\031]+(?:(?:(?:\\r\\n)?[ \\t])+|\\Z|(?=[\\[\"()<>@,;:\\\\\".\\[\\]]))|\\[([^\\[\\]\\r\\\\]|\\\\.)*\\](?:(?:\\r\\n)?[ \\t])*))*)*:(?:(?:\\r\\n)?[ \\t])*)?(?:[^()<>@,;:\\\\\".\\[\\] \\000-\\031]+(?:(?:(?:\\r\\n)?[ \\t])+|\\Z|(?=[\\[\"()<>@,;:\\\\\".\\[\\]]))|\"(?:[^\\\"\\r\\\\]|\\\\.|(?:(?:\\r\\n)?[ \\t]))*\"(?:(?:\\r\\n)?[ \\t])*)(?:\\.(?:(?:\\r\\n)?[ \\t])*(?:[^()<>@,;:\\\\\".\\[\\] \\000-\\031]+(?:(?:(?:\\r\\n)?[ \\t])+|\\Z|(?=[\\[\"()<>@,;:\\\\\".\\[\\]]))|\"(?:[^\\\"\\r\\\\]|\\\\.|(?:(?:\\r\\n)?[ \\t]))*\"(?:(?:\\r\\n)?[ \\t])*))*@(?:(?:\\r\\n)?[ \\t])*(?:[^()<>@,;:\\\\\".\\[\\] \\000-\\031]+(?:(?:(?:\\r\\n)?[ \\t])+|\\Z|(?=[\\[\"()<>@,;:\\\\\".\\[\\]]))|\\[([^\\[\\]\\r\\\\]|\\\\.)*\\](?:(?:\\r\\n)?[ \\t])*)(?:\\.(?:(?:\\r\\n)?[ \\t])*(?:[^()<>@,;:\\\\\".\\[\\] \\000-\\031]+(?:(?:(?:\\r\\n)?[ \\t])+|\\Z|(?=[\\[\"()<>@,;:\\\\\".\\[\\]]))|\\[([^\\[\\]\\r\\\\]|\\\\.)*\\](?:(?:\\r\\n)?[ \\t])*))*\\>(?:(?:\\r\\n)?[ \\t])*)(?:,\\s*(?:(?:[^()<>@,;:\\\\\".\\[\\] \\000-\\031]+(?:(?:(?:\\r\\n)?[ \\t])+|\\Z|(?=[\\[\"()<>@,;:\\\\\".\\[\\]]))|\"(?:[^\\\"\\r\\\\]|\\\\.|(?:(?:\\r\\n)?[ \\t]))*\"(?:(?:\\r\\n)?[ \\t])*)(?:\\.(?:(?:\\r\\n)?[ \\t])*(?:[^()<>@,;:\\\\\".\\[\\] \\000-\\031]+(?:(?:(?:\\r\\n)?[ \\t])+|\\Z|(?=[\\[\"()<>@,;:\\\\\".\\[\\]]))|\"(?:[^\\\"\\r\\\\]|\\\\.|(?:(?:\\r\\n)?[ \\t]))*\"(?:(?:\\r\\n)?[ \\t])*))*@(?:(?:\\r\\n)?[ \\t])*(?:[^()<>@,;:\\\\\".\\[\\] \\000-\\031]+(?:(?:(?:\\r\\n)?[ \\t])+|\\Z|(?=[\\[\"()<>@,;:\\\\\".\\[\\]]))|\\[([^\\[\\]\\r\\\\]|\\\\.)*\\](?:(?:\\r\\n)?[ \\t])*)(?:\\.(?:(?:\\r\\n)?[ \\t])*(?:[^()<>@,;:\\\\\".\\[\\] \\000-\\031]+(?:(?:(?:\\r\\n)?[ \\t])+|\\Z|(?=[\\[\"()<>@,;:\\\\\".\\[\\]]))|\\[([^\\[\\]\\r\\\\]|\\\\.)*\\](?:(?:\\r\\n)?[ \\t])*))*|(?:[^()<>@,;:\\\\\".\\[\\] \\000-\\031]+(?:(?:(?:\\r\\n)?[ \\t])+|\\Z|(?=[\\[\"()<>@,;:\\\\\".\\[\\]]))|\"(?:[^\\\"\\r\\\\]|\\\\.|(?:(?:\\r\\n)?[ \\t]))*\"(?:(?:\\r\\n)?[ \\t])*)*\\<(?:(?:\\r\\n)?[ \\t])*(?:@(?:[^()<>@,;:\\\\\".\\[\\] \\000-\\031]+(?:(?:(?:\\r\\n)?[ \\t])+|\\Z|(?=[\\[\"()<>@,;:\\\\\".\\[\\]]))|\\[([^\\[\\]\\r\\\\]|\\\\.)*\\](?:(?:\\r\\n)?[ \\t])*)(?:\\.(?:(?:\\r\\n)?[ \\t])*(?:[^()<>@,;:\\\\\".\\[\\] \\000-\\031]+(?:(?:(?:\\r\\n)?[ \\t])+|\\Z|(?=[\\[\"()<>@,;:\\\\\".\\[\\]]))|\\[([^\\[\\]\\r\\\\]|\\\\.)*\\](?:(?:\\r\\n)?[ \\t])*))*(?:,@(?:(?:\\r\\n)?[ \\t])*(?:[^()<>@,;:\\\\\".\\[\\] \\000-\\031]+(?:(?:(?:\\r\\n)?[ \\t])+|\\Z|(?=[\\[\"()<>@,;:\\\\\".\\[\\]]))|\\[([^\\[\\]\\r\\\\]|\\\\.)*\\](?:(?:\\r\\n)?[ \\t])*)(?:\\.(?:(?:\\r\\n)?[ \\t])*(?:[^()<>@,;:\\\\\".\\[\\] \\000-\\031]+(?:(?:(?:\\r\\n)?[ \\t])+|\\Z|(?=[\\[\"()<>@,;:\\\\\".\\[\\]]))|\\[([^\\[\\]\\r\\\\]|\\\\.)*\\](?:(?:\\r\\n)?[ \\t])*))*)*:(?:(?:\\r\\n)?[ \\t])*)?(?:[^()<>@,;:\\\\\".\\[\\] \\000-\\031]+(?:(?:(?:\\r\\n)?[ \\t])+|\\Z|(?=[\\[\"()<>@,;:\\\\\".\\[\\]]))|\"(?:[^\\\"\\r\\\\]|\\\\.|(?:(?:\\r\\n)?[ \\t]))*\"(?:(?:\\r\\n)?[ \\t])*)(?:\\.(?:(?:\\r\\n)?[ \\t])*(?:[^()<>@,;:\\\\\".\\[\\] \\000-\\031]+(?:(?:(?:\\r\\n)?[ \\t])+|\\Z|(?=[\\[\"()<>@,;:\\\\\".\\[\\]]))|\"(?:[^\\\"\\r\\\\]|\\\\.|(?:(?:\\r\\n)?[ \\t]))*\"(?:(?:\\r\\n)?[ \\t])*))*@(?:(?:\\r\\n)?[ \\t])*(?:[^()<>@,;:\\\\\".\\[\\] \\000-\\031]+(?:(?:(?:\\r\\n)?[ \\t])+|\\Z|(?=[\\[\"()<>@,;:\\\\\".\\[\\]]))|\\[([^\\[\\]\\r\\\\]|\\\\.)*\\](?:(?:\\r\\n)?[ \\t])*)(?:\\.(?:(?:\\r\\n)?[ \\t])*(?:[^()<>@,;:\\\\\".\\[\\] \\000-\\031]+(?:(?:(?:\\r\\n)?[ \\t])+|\\Z|(?=[\\[\"()<>@,;:\\\\\".\\[\\]]))|\\[([^\\[\\]\\r\\\\]|\\\\.)*\\](?:(?:\\r\\n)?[ \\t])*))*\\>(?:(?:\\r\\n)?[ \\t])*))*)?;\\s*)");
		return email.matcher(s).matches();
	}
	
	public boolean checkValidPassword(String s) {
		Pattern password = Pattern.compile("^(?=.*[a-z])(?=.*[A-Z]).{8,}$");
		return password.matcher(s).matches();
	}
	
	public boolean checkValidPhone(String s) {
		Pattern phone1 = Pattern.compile("[0][8-9][0-9^5]\\d{7}");
		Pattern phone2 = Pattern.compile("[0][1][2-9^3,4,5,7][0-9]\\d{7}");
		if (!phone1.matcher(s).matches() && !phone2.matcher(s).matches()) {
			return false;
		}
		return true;
	}
	
	
	

public void updateSubscription(EntityService<UserTable> userService,
		EntityService<PackageTable> packageService, EntityService<SubscriptionTable> subService,
		EntityService<TransactionTable> transService, int userID,
		int packageID, int subscriptionID, int transType) {
	extendExpiredDate(userID, packageService.findById(packageID).getDaysOfUsing(), subscriptionID,
			subService);
	doTransactionTable(userID, transType, userService, transService, packageService.findById(packageID));

}

// extend expired date
public void extendExpiredDate(int userID, int numberOfDays, int subscriptionID,
		EntityService<SubscriptionTable> subTable) {
	SubscriptionTable userSubTable = null;
	userSubTable = subTable.findById(subscriptionID);

	String date = userSubTable.getExpiredDate().toString();
	SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
	Calendar calendar = Calendar.getInstance();
	try {
		Date userCurrentDate = sdf.parse(date);
		String date1 = sdf.format(Calendar.getInstance().getTime());

		Date currentDate = sdf.parse(date1);

		if (!userCurrentDate.after(currentDate)) {
			date = date1;
		}
		calendar.setTime(sdf.parse(date));
		calendar.add(Calendar.DATE, numberOfDays);
		java.sql.Date dateToAdd = new java.sql.Date(calendar.getTimeInMillis());
		userSubTable.setExpiredDate(dateToAdd);
		subTable.update(userSubTable);
	} catch (ParseException e) {
		e.printStackTrace();
	}

}

// update transaction table
public void doTransactionTable(int userID, int transType, EntityService<UserTable> userService,
		EntityService<TransactionTable> transService, PackageTable pack) {
	TransactionTable transEntity = new TransactionTable();
	transEntity.setUserTable(userService.findById(userID));
	transEntity.setTransactionType(transType);
	Calendar calendar = Calendar.getInstance();
	java.sql.Date dateToAdd = new java.sql.Date(calendar.getTimeInMillis());
	transEntity.setTransactionnDate(dateToAdd);
	transEntity.setTransactionPackagePrice(pack.getPackagePrice());
	transEntity.setTransactionPackageName(pack.getPackageName());
	transEntity.setTransactionPackageDays(pack.getDaysOfUsing());
	transService.save(transEntity);
	}



public void sendEmail(String emailto, String subject, String content) {
    // Recipient's email ID needs to be mentioned.
    String to = emailto;

    // Sender's email ID needs to be mentioned
    String from = Common_const.emailUserName;

    // Assuming you are sending email from localhost

    // Get system properties
    Properties properties = System.getProperties();

    // Setup mail server
    properties.setProperty("mail.smtp.host", Common_const.smtpHost);

    // Get the default Session object.

    properties.put("mail.smtp.starttls.enable", true);
    properties.put("mail.smtp.port", "587");
    properties.setProperty("mail.smtp.user", Common_const.emailUserName);
    properties.setProperty("mail.smtp.password", Common_const.emailPassword);
    properties.setProperty("mail.smtp.auth", "true");


    Session session = Session.getDefaultInstance(properties, new javax.mail.Authenticator() {
        protected PasswordAuthentication getPasswordAuthentication() {
            return new PasswordAuthentication(Common_const.emailUserName, Common_const.emailPassword);
        }
    });
    try {
        // Create a default MimeMessage object.
        MimeMessage message = new MimeMessage(session);

        // Set From: header field of the header.
        message.setFrom(new InternetAddress(from));

        // Set To: header field of the header.
        message.addRecipient(Message.RecipientType.TO, new InternetAddress(to));

        // Set Subject: header field
        message.setSubject(subject);

        // Now set the actual message
        message.setText(content);

        // Send message
        Transport.send(message);
    } catch (Exception mex) {
        mex.printStackTrace();
    }
}

}


