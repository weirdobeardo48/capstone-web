package capstone.common;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Base64;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class common_WP implements CommonInterface{

    public int getProductCount(String shopUrl, String shopApiKey, String password) {
        if (!shopUrl.startsWith("https://")) {
            shopUrl = "https://" + shopUrl;
        }
        shopUrl = shopUrl + "/wp-json/wc/v2/products";
        try {
            URL url = new URL(shopUrl);
            String encoding = Base64.getEncoder().encodeToString((shopApiKey + ":" + password).getBytes("UTF-8"));

            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("GET");
            connection.setDoOutput(true);
            connection.setRequestProperty("Authorization", "Basic " + encoding);
            Map<String, List<String>> map = connection.getHeaderFields();
            if (map.containsKey("X-WP-Total")) {
                return Integer.parseInt(map.get("X-WP-Total").get(0));
            }
        } catch (Exception e) {
            e.printStackTrace();
            return -1;
        }
        return -1;
    }

    public String getDomain(String shopUrl, String shopApiKey, String password) {
        return shopUrl;
    }

    public Map<String, String> getAllCollection(String shopUrl, String shopApiKey, String password) {
        JSONParser parser = new JSONParser();
        if (!shopUrl.startsWith("https://")) {
            shopUrl = "https://" + shopUrl;
        }
        Map<String, String> collection = new HashMap<>();
        if (!shopUrl.startsWith("https://")) {
            shopUrl = "https://" + shopUrl;
        }
        shopUrl = shopUrl + "/wp-json/wc/v2/products/categories";
        try {
            URL url = new URL(shopUrl);
            String encoding = Base64.getEncoder().encodeToString((shopApiKey + ":" + password).getBytes("UTF-8"));

            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("GET");
            connection.setDoOutput(true);
            connection.setRequestProperty("Authorization", "Basic " + encoding);
            InputStream content = connection.getInputStream();
            BufferedReader in = new BufferedReader(new InputStreamReader(content));
            String line;
            while ((line = in.readLine()) != null) {
                JSONArray jsonArray = (JSONArray) parser.parse(line);
                for (Object aJsonArray : jsonArray) {
                    JSONObject jsonObject = (JSONObject) parser.parse(aJsonArray.toString());
                    collection.put(jsonObject.get("id").toString(), jsonObject.get("name").toString());
                }
            }
        } catch (IOException | ParseException e) {
            e.printStackTrace();
        }
        return collection;
    }
}
