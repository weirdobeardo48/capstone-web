package capstone.common;

public class Common_const {
	public static final int isADMIN = 2;
	public static final int isUSER = 1;
	public static final int isROOT = 0;
	public static final boolean USER_IS_NOT_LOCKED = true;
	

	public static final String PRODUCT_NAME = "SOMETHING ELSE";
	public static final String RETURN_URL = "http://easyshoppingfeed.com";
	public static final String RETURN_URL_SANDBOX_VPS="http://easyshoppingfeed.com:8081";
	public static final String RETURN_URL_SANDBOX = "http://localhost:8080/Capstone-Web";
	public static final String COMMENT = "This is new product";

	// account - order description - product name - product price - quantity - total
	// price - url cancel - url detail - url success
	public static final String BAOKIM_API = "https://www.baokim.vn/payment/product/version11?business=%s&id=&order_description=%s&product_name=%s&product_price=%s&product_quantity=%s&total_amount=%s&url_cancel=%s&url_detail=%s&url_success=%s";
	public static final String BAOKIM_ACCOUNT = "billing@easyshoppingfeed.com";
	public static final String PRODUCT_QUANTITY = "1";
	public static final String URL_CANCEL = "http://easyshoppingfeed.com/price.xyz";
	public static final String URL_DETAIL = "http://easyshoppingfeed.com/price.xyz";

	// Main Nganluong.vn
	public static final String NGANLUONG_API = "https://www.nganluong.vn/button_payment.php?receiver=%s&product_name=%s&price=%s&return_url=%s&comments=%s";
	public static final String NGANLUONG_ACCOUNT = "billing@easyshoppingfeed.com";


	// Sandbox Nganluong
	public static final String NGANLUONG_API_SANDBOX = "https://sandbox.nganluong.vn:8088/nl30/button_payment.php?receiver=%s&product_name=%s&price=%s&return_url=%s&comments=%s";
	public static final String NGANLUONG_ACCOUNT_SANDBOX = "billing-sandbox@easyshoppingfeed.com";

	
	//Transtype
	public static final int NGANLUONG_TRANSACTION_TYPE = 1;
	public static final int BAOKIM_TRANSACTION_TYPE = 2;
	public static final int BANK_TRANSFER = 3;
	
	//Email
	public static final String sendToEmail = "";
	public static final String emailUserName = "capstone@easyshoppingfeed.com";
	public static final String emailPassword = "123456789";	
	public static final String smtpHost = "smtp.1and1.com";
	

}
