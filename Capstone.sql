DROP database IF EXISTS Capstone;
create database Capstone;
use Capstone;

create table userTable(
userID int not null auto_increment,
userName varchar(100) not null,
userPassword varchar(100) not null,
userPhone varchar(20),
userEmail varchar(100),
userType int not null,
isActive bit,
primary key(userID)
);

create table packageTable(
packageID int not null auto_increment,
packageName varchar(100) not null,
packageDescription varchar(100),
packagePrice int not null,
DaysOfUsing int not null,
primary key(packageID)
);

create table transactionTable(
transactionID int not null auto_increment,
userID int,
transactionType int not null,
transactionnDate date not null,
transactionPackageName varchar(100) not null, 
transactionPackagePrice int not null,
transactionPackageDays int not null,
primary key(transactionID)
);

create table subscriptionTable(
subscriptionID int not null auto_increment,
userID int,
expiredDate date not null,
isExpired bit not null,
primary key(subscriptionID)
);

create table Application_Version(
appVersionID int not null auto_increment,
appVersionNumber varchar(10) not null,
changeLog text,
appLink text,
primary key(appVersionID)
);

create table Data_Version(
dataVersionID int not null auto_increment,
dataVersionNumber varchar(10) not null,
primary key(dataVersionID)
);

alter table subscriptionTable
ADD constraint FK_Subscription_userId_User_userId
foreign key (userID) references userTable(userID)
on update cascade
on delete cascade;

alter table transactionTable
ADD constraint FK_Transaction_userId_User_userId
foreign key (userID) references userTable(userID)
on update cascade
on delete cascade;

